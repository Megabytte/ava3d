//
// Created by Keith Webb on 11/24/15.
//

#include <Ava/Graphics/3D/Camera3D.h>
#include <glm/gtc/matrix_transform.hpp>
#include <Ava/Graphics/Shader.h>
#include <Ava/Graphics/Display.h>
#include <Ava/Util/System.h>
#include <Ava/Util/InputManager.h>
#include <Ava/Game.h>

Camera3D::Camera3D()
{
    this->position.x = 0;
    this->position.y = 0;
    this->position.z = 0;
    scale.x = scale.y = scale.z = 1.0f;

    glm::vec3 cameraPos = position;
    cameraFront = glm::vec3(0.0f, 0.0f, -1.0f);
    cameraUp = glm::vec3(0.0f, 1.0f,  0.0f);

    view = glm::lookAt(cameraPos, cameraPos + cameraFront, cameraUp);
    System::SetMousePosition(Display::Width / 2, Display::Height / 2);
    System::HideMouse();

    lmx = Display::Width / 2;
    lmy = Display::Height / 2;
}

Camera3D::Camera3D(float x, float y, float z, float width, float height, float near, float far)
{
    set(x, y, z, width, height, near, far);
}

Camera3D::~Camera3D()
{

}

void Camera3D::set(float x, float y, float z, float width, float height, float near, float far)
{
    position.x = x;
    position.y = y;
    position.z = z;
    this->width = width;
    this->height = height;
    this->near = near;
    this->far = far;

    System::SetMousePosition(Display::Width / 2, Display::Height / 2);
    System::HideMouse();

    lmx = Display::Width / 2;
    lmy = Display::Height / 2;
}

void Camera3D::update()
{
    glm::vec2 mPos = InputManager::GetMousePosition();

    if(firstMouse && mPos.x != lmx && mPos.y != lmy)
    {
        lmx = mPos.x;
        lmy = mPos.y;
        firstMouse = false;
    }

    float xOffset = mPos.x - lmx;
    float yOffset = lmy - mPos.y;
    lmx = mPos.x;
    lmy = mPos.y;

    float sensitivity = 0.05f;
    xOffset *= sensitivity;
    yOffset *= sensitivity;

    yaw += xOffset;
    pitch += yOffset;

    if(pitch > 89.0f)
        pitch =  89.0f;
    if(pitch < -89.0f)
        pitch = -89.0f;

    glm::vec3 front;
    front.x = (float) (cos(glm::radians(pitch)) * cos(glm::radians(yaw)));
    front.y = (float) sin(glm::radians(pitch));
    front.z = (float) (cos(glm::radians(pitch)) * sin(glm::radians(yaw)));
    cameraFront = glm::normalize(front);

    cameraUp = glm::vec3(0.0f, 1.0f,  0.0f);

    view = glm::translate(view, glm::vec3(0.5f * width, 0.5f * height, 0.0f));
    //view = glm::rotate(view, rotation, glm::vec3(0, 0, 0));
    view = glm::scale(view, scale);
    view = glm::translate(view, glm::vec3(-0.5f * width, -0.5f * height, 0.0f));

    view = glm::lookAt(position, position + cameraFront, cameraUp);

    System::SetMousePosition(Display::Width / 2, Display::Height / 2);
    mPos = InputManager::GetMousePosition();
    lmx = mPos.x;
    lmy = mPos.y;
}

void Camera3D::apply(Shader* shader) {
    shader->Use();
    shader->SetMatrix4("view", view);
    shader->SetMatrix4("projection", Shader::Global_Perspective_Projection);
    shader->SetVector3f("viewPos", position);
}
