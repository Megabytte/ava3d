//
// Created by Keith Webb on 11/18/15.
//

#include <Ava/Graphics/2D/Sprite.h>

Sprite::Sprite()
{
    mTexture = nullptr;
    VBO = VAO = 0;
    scale.x = scale.y = 1.0f;
    color.r = color.g = color.b = color.a = 1.0f;
}

Sprite::Sprite(Texture *texture)
{
    VBO = VAO = 0;
    scale.x = scale.y = 1.0f;
    color.r = color.g = color.b = color.a = 1.0f;
    setTexture(texture);
}

Sprite::~Sprite()
{
    mTexture = nullptr;
    glDeleteVertexArrays(1, &VAO);
    glDeleteBuffers(1, &VBO);
}

void Sprite::setTexture(Texture *texture)
{
    mTexture = texture;
    setup();
}

Texture *Sprite::getTexture()
{
    return mTexture;
}

void Sprite::render(Shader* shader)
{
    glDisable(GL_DEPTH_TEST);

    shader->Use();
    glm::mat4 model = getTransform();

    model = glm::scale(model, glm::vec3(mTexture->Width, mTexture->Height, 1.0f));

    // Get matrix's uniform location and set matrix
    shader->SetMatrix4("model", model);
    // Get color's uniform location and set color
    shader->SetVector4f("color1", color);
    shader->SetBool("isFont", (GLboolean) false);

    shader->SetInteger("image", 0);
    glActiveTexture(GL_TEXTURE0);
    mTexture->Bind();

    glBindVertexArray(VAO);
    glDrawArrays(GL_TRIANGLES, 0, 6);
    glBindVertexArray(0);

    mTexture->Unbind();

    glEnable(GL_DEPTH_TEST);
}

void Sprite::setup()
{
    // Configure VAO/VBO
    GLfloat vertices[] = {
            // Pos      // Tex
            0.0f, 1.0f, 0.0f, 1.0f,
            1.0f, 0.0f, 1.0f, 0.0f,
            0.0f, 0.0f, 0.0f, 0.0f,

            0.0f, 1.0f, 0.0f, 1.0f,
            1.0f, 1.0f, 1.0f, 1.0f,
            1.0f, 0.0f, 1.0f, 0.0f
    };

    glGenVertexArrays(1, &VAO);
    glGenBuffers(1, &VBO);

    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    glBindVertexArray(VAO);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), (GLvoid*)0);

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);

    scale.x = scale.y = 1.0f;
    color.r = color.g = color.b = color.a = 1.0f;
}
