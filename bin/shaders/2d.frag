#version 330 core

in vec2 TexCoords;
out vec4 color;

uniform bool isFont;
uniform sampler2D image;
uniform vec4 color1;

void main()
{
    if(isFont)
    {
        vec4 sampled = vec4(1.0, 1.0, 1.0, texture(image, TexCoords).r);
        color = color1 * sampled;
    }
    else
    {
        color = color1 * texture(image, TexCoords);
    }
}