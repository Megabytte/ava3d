//
// Created by Keith Webb on 11/26/15.
//

#pragma once


#include <Ava/Graphics/Colorable.h>
#include <Ava/Graphics/3D/Transformable3D.h>
#include <Ava/Graphics/Shader.h>
#include <vector>

class Light : public Transformable3D, public Colorable
{
public:
    static int MAX_LIGHTS;
    static int USED_LIGHTS;
    static std::vector<Light*> lights;

    enum LightType {POINT, SPOT, DIRECTIONAL};

    Light(LightType type = POINT);
    ~Light();

    void setLightType(LightType type);
    LightType getLightType();

    float getCutOff();
    float getOuterCutOff();
    void setCutOff(float v);
    void setOuterCutOff(float v);
    float getConstant();
    void setConstant(float constant);
    float getLinear();
    void setLinear(float linear);
    float getQuadratic();
    void setQuadratic(float quadratic);
    void setDirectionX(float dirX);
    void setDirectionY(float dirY);
    void setDirectionZ(float dirZ);
    void setDirection(glm::vec3 dir);
    void setDirection(float dirX, float dirY, float dirZ);
    glm::vec3 getDirection();

    void render(Shader* shader);

private:
    std::string id;

    std::string getCommand(std::string command);

    int lightNum;
    float cutoff;
    float outercutoff;
    glm::vec3 direction;

    float constant, linear, quadratic;
    void setup();
    GLuint VAO, VBO;
    LightType mType;
};
