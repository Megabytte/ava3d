//
// Created by Keith Webb on 11/28/15.
//

#include <Ava/Graphics/3D/Model.h>
#include <iostream>
#include <SDL2/SDL_surface.h>
#include <SDL2/SDL_image.h>

Model::Model(std::string path)
{
    this->loadModel(path.c_str());
}

void Model::Draw(Shader* shader)
{
    glCullFace(GL_BACK);
    glFrontFace(GL_CCW);
    for(GLuint i = 0; i < this->meshes.size(); i++)
        this->meshes[i].Draw(shader);
}

void Model::loadModel(std::string path)
{
    Assimp::Importer import;
    const aiScene* scene = import.ReadFile(path, aiProcess_Triangulate);

    if(!scene || scene->mFlags == AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode)
    {
        std::cout << "ERROR::ASSIMP::" << import.GetErrorString() << std::endl;
        return;
    }
    this->directory = path.substr(0, path.find_last_of('/'));

    this->processNode(scene->mRootNode, scene);
}

void Model::processNode(aiNode *node, const aiScene *scene)
{
    // Process all the node's meshes (if any)
    for(GLuint i = 0; i < node->mNumMeshes; i++)
    {
        aiMesh* mesh = scene->mMeshes[node->mMeshes[i]];
        this->meshes.push_back(this->processMesh(mesh, scene));
    }
    // Then do the same for each of its children
    for(GLuint i = 0; i < node->mNumChildren; i++)
    {
        this->processNode(node->mChildren[i], scene);
    }
}

Mesh Model::processMesh(aiMesh *mesh, const aiScene *scene)
{
    std::vector<Mesh::Vertex> vertices;
    std::vector<GLuint> indices;
    std::vector<Mesh::Texture> textures;

    for(GLuint i = 0; i < mesh->mNumVertices; i++)
    {
        Mesh::Vertex vertex;
        // Process vertex positions, normals and texture coordinates
        glm::vec3 vector;
        vector.x = mesh->mVertices[i].x;
        vector.y = mesh->mVertices[i].y;
        vector.z = mesh->mVertices[i].z;
        vertex.Position = vector;

        vector.x = mesh->mNormals[i].x;
        vector.y = mesh->mNormals[i].y;
        vector.z = mesh->mNormals[i].z;
        vertex.Normal = vector;

        if(mesh->mTextureCoords[0]) // Does the mesh contain texture coordinates?
        {
            glm::vec2 vec;
            vec.x = mesh->mTextureCoords[0][i].x;
            vec.y = mesh->mTextureCoords[0][i].y;
            vertex.TexCoords = vec;
        }
        else
            vertex.TexCoords = glm::vec2(0.0f, 0.0f);

        vertices.push_back(vertex);
    }
    // Process indices
    for(GLuint i = 0; i < mesh->mNumFaces; i++)
    {
        aiFace face = mesh->mFaces[i];
        for(GLuint j = 0; j < face.mNumIndices; j++)
            indices.push_back(face.mIndices[j]);
    }

    // Process material
    if(mesh->mMaterialIndex >= 0)
    {
        aiMaterial* material = scene->mMaterials[mesh->mMaterialIndex];
        std::vector<Mesh::Texture> diffuseMaps = this->loadMaterialTextures(material, aiTextureType_DIFFUSE, "texture_diffuse");
        textures.insert(textures.end(), diffuseMaps.begin(), diffuseMaps.end());
        std::vector<Mesh::Texture> specularMaps = this->loadMaterialTextures(material, aiTextureType_SPECULAR, "texture_specular");
        textures.insert(textures.end(), specularMaps.begin(), specularMaps.end());
    }

    return Mesh(vertices, indices, textures, this);
}

std::vector<Mesh::Texture> Model::loadMaterialTextures(aiMaterial *mat, aiTextureType type, std::string typeName)
{
    std::vector<Mesh::Texture> textures;
    for(GLuint i = 0; i < mat->GetTextureCount(type); i++)
    {
        aiString str;
        mat->GetTexture(type, i, &str);
        GLboolean skip = (GLboolean) false;
        for(GLuint j = 0; j < textures_loaded.size(); j++)
        {
            if(textures_loaded[j].path == str)
            {
                textures.push_back(textures_loaded[j]);
                skip = (GLboolean) true;
                break;
            }
        }
        if(!skip)
        {   // If texture hasn't been loaded already, load it
            Mesh::Texture texture;
            texture.id = TextureFromFile(str, this->directory);
            texture.type = typeName;
            texture.path = str;
            textures.push_back(texture);
            this->textures_loaded.push_back(texture);  // Add to loaded textures
        }
    }
    return textures;
}

GLuint Model::TextureFromFile(aiString file, std::string dir)
{
    GLuint ID;
    // Texture image dimensions
    GLuint Width, Height; // Width and Height of loaded image in pixels
    // Texture Format
    GLuint Internal_Format = GL_RGB; // Format of texture object
    GLuint Image_Format = GL_RGB; // Format of loaded image
    // Texture configuration
    GLuint Wrap_S = GL_REPEAT; // Wrapping mode on S axis
    GLuint Wrap_T = GL_REPEAT; // Wrapping mode on T axis
    GLuint Filter_Min = GL_LINEAR; // Filtering mode if texture pixels < screen pixels
    GLuint Filter_Max = GL_LINEAR; // Filtering mode if texture pixels > screen pixels

    aiString path(dir);
    path.Append("/");
    path.Append(file.C_Str());

    SDL_Surface* Surface = IMG_Load(path.C_Str());

    glGenTextures(1, &ID);
    glBindTexture(GL_TEXTURE_2D, ID);

    if(Surface->format->BytesPerPixel == 4) {
        Internal_Format = GL_RGBA;
        Image_Format = GL_RGBA;
    }

    Width = (GLuint) Surface->w;
    Height = (GLuint) Surface->h;

    glTexImage2D(GL_TEXTURE_2D, 0, Internal_Format, Width, Height, 0, Image_Format, GL_UNSIGNED_BYTE, Surface->pixels);
    glGenerateMipmap(GL_TEXTURE_2D);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, Wrap_S);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, Wrap_T);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, Filter_Min);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, Filter_Max);

    glBindTexture(GL_TEXTURE_2D, 0);

    SDL_FreeSurface(Surface);

    return ID;
}
