//
// Created by Keith Webb on 11/18/15.
//

#pragma once

#include <Ava/Util/GameState.h>
#include <Ava/Util/EventReciever.h>
#include <Ava/Graphics/3D/Camera3D.h>
#include <Ava/Graphics/3D/Cube.h>
#include <Ava/Graphics/3D/Light.h>
#include <Ava/Graphics/3D/Model.h>
#include <Ava/Graphics/2D/Sprite.h>
#include <Ava/Graphics/2D/Font.h>
#include <Ava/Graphics/RenderTexture.h>

class MainState : public GameState, public EventReciever
{
public:
    MainState();
    ~MainState();

    virtual void create();
    virtual void update(float dt);
    virtual void render();
    virtual void event(std::string eventType, std::string eventValue);

private:
    Camera3D camera3D;
    Camera2D camera2D;
    Cube test[10];
    Light spot;
    Light point;
    Light directional;

    Model model;

    Sound* sound;

    Sprite sprite;
    Sprite face;
    RenderTexture renderTexture;

    Font font;

    float rt = 0.0f;

    float testX;
    bool m;

    Shader *shader3D, *shader2D;
    float fov = 45.0f;
};
