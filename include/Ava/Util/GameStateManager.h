//
// Created by Keith Webb on 11/18/15.
//

#pragma once

#include <stack>

class Game;
class GameState;

class GameStateManager
{
public:
    static Game* game;
    static void PushState(GameState*);
    static void PopState();
    static void ChangeState(GameState*);
    static GameState* PeekState();

    static void UpdateStates(float);
    static void RenderStates();

private:
    GameStateManager();
    ~GameStateManager();
    static std::stack<GameState*> stack;
};
