//
// Created by Keith Webb on 11/23/15.
//

#include <Ava/Util/System.h>

#include <SDL2/SDL_net.h>
#include <fmodex/fmod_errors.h>

FMOD::System* System::FMOD_System = nullptr;
FT_Library System::ft;

void System::Init()
{
    InitSDL();
    InitSDL_Image();
    InitSDL_Net();
    InitFMOD();
    InitFreetype();
}

void System::DeInit()
{
    DeInitFMOD();
    DeInitSDL_Net();
    DeInitSDL_Image();
    DeInitSDL();
    DeInitFreetype();
}

void System::ShowMouse()
{
    SDL_ShowCursor(1);
}

void System::HideMouse()
{
    SDL_ShowCursor(0);
}

void System::SetMousePosition(int x, int y)
{
    SDL_WarpMouseInWindow(NULL, x, y);
}

void System::InitSDL()
{
    if (SDL_Init(SDL_INIT_VIDEO) < 0)
        exit(-1);

    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
}

void System::InitSDL_Image(int flags)
{
    int initted = IMG_Init(flags);
    if((initted & flags) != flags) {
        printf("IMG_Init: Failed to init required jpg and png support!\n");
        printf("IMG_Init: %s\n", IMG_GetError());

        SDL_Quit();
        exit(-1);
    }
}

void System::InitSDL_Net()
{
    if(SDLNet_Init() == -1)
    {
        SDL_Quit();
        exit(-1);
    }
}

void System::InitFMOD()
{
    FMOD_RESULT result;
    FMOD_System = NULL;

    result = FMOD::System_Create(&FMOD_System);      // Create the main FMOD_System object.
    if (result != FMOD_OK)
    {
        printf("FMOD error! (%d) %s\n", result, FMOD_ErrorString(result));
        exit(-1);
    }

    result = FMOD_System->init(512, FMOD_INIT_NORMAL, 0);    // Initialize FMOD.
    if (result != FMOD_OK)
    {
        printf("FMOD error! (%d) %s\n", result, FMOD_ErrorString(result));
        exit(-1);
    }
}

void System::InitFreetype()
{
    if (FT_Init_FreeType(&ft))
        printf("ERROR::FREETYPE: Could not init FreeType Library!\n");
}

void System::DeInitSDL()
{
    SDL_Quit();
}

void System::DeInitSDL_Image()
{
    IMG_Quit();
}

void System::DeInitSDL_Net()
{
    SDLNet_Quit();
}

void System::DeInitFMOD()
{
    FMOD_System->release();
}

void System::DeInitFreetype()
{
    FT_Done_FreeType(ft);
}
