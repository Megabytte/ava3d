//
// Created by Keith Webb on 11/29/15.
//

#include <Ava/Graphics/2D/Font.h>
#include <Ava/Util/System.h>
#include <glm/gtc/matrix_transform.hpp>

Font::Font()
{
    canRender = false;
}

Font::Font(std::string path, unsigned int size)
{
    loadFromFile(path, size);
}

Font::~Font()
{
    glDeleteVertexArrays(1, &VAO);
    glDeleteBuffers(1, &VBO);
}

void Font::loadFromFile(std::string path, unsigned int size)
{
    if (FT_New_Face(System::ft, "fonts/arial.ttf", 0, &face))
        printf("ERROR::FREETYPE: Failed to load font!\n");
    else
        canRender = true;

    FT_Set_Pixel_Sizes(face, 0, size);
    fontSize = size;

    glPixelStorei(GL_UNPACK_ALIGNMENT, 1); // Disable byte-alignment restriction

    for (GLubyte c = 0; c < 128; c++)
    {
        // Load character glyph
        if (FT_Load_Char(face, c, FT_LOAD_RENDER))
        {
            printf("ERROR::FREETYTPE: Failed to load Glyph!\n");
            continue;
        }
        // Generate texture
        GLuint texture;
        glGenTextures(1, &texture);
        glBindTexture(GL_TEXTURE_2D, texture);
        glTexImage2D(
                GL_TEXTURE_2D,
                0,
                GL_RED,
                face->glyph->bitmap.width,
                face->glyph->bitmap.rows,
                0,
                GL_RED,
                GL_UNSIGNED_BYTE,
                face->glyph->bitmap.buffer
        );
        // Set texture options
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        // Now store character for later use
        Character character = {
                texture,
                glm::ivec2(face->glyph->bitmap.width, face->glyph->bitmap.rows),
                glm::ivec2(face->glyph->bitmap_left, face->glyph->bitmap_top),
                (GLuint) face->glyph->advance.x
        };
        Characters.insert(std::pair<GLchar, Character>(c, character));
    }

    glGenVertexArrays(1, &VAO);
    glGenBuffers(1, &VBO);
    glBindVertexArray(VAO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 6 * 4, NULL, GL_DYNAMIC_DRAW);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), 0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);

    FT_Done_Face(face);
}

void Font::render(Shader &s, std::string text, glm::vec2 position, float rotation, glm::vec2 scale, glm::vec2 center, glm::vec4 color)
{
    if(!canRender)
        return;

    // Activate corresponding render state
    s.Use();
    s.SetVector4f("color1", color);
    s.SetInteger("image", 0);
    s.SetBool("isFont", (GLboolean) true);
    glActiveTexture(GL_TEXTURE0);
    glBindVertexArray(VAO);

    // Iterate through all characters
    std::string::const_iterator c;
    for (c = text.begin(); c != text.end(); c++)
    {
        Character ch = Characters[*c];

        GLfloat xpos = ch.Bearing.x;
        GLfloat ypos = (ch.Size.y - ch.Bearing.y);

        GLfloat w = ch.Size.x;
        GLfloat h = ch.Size.y;
        // Update for each character
        GLfloat vertices[6][4] = {
                { xpos,     ypos + h,   0.0, 0.0 },
                { xpos,     ypos,       0.0, 1.0 },
                { xpos + w, ypos,       1.0, 1.0 },

                { xpos,     ypos + h,   0.0, 0.0 },
                { xpos + w, ypos,       1.0, 1.0 },
                { xpos + w, ypos + h,   1.0, 0.0 }
        };
        // Render glyph texture over quad
        glBindTexture(GL_TEXTURE_2D, ch.TextureID);
        // Update content of VBO memory
        glBindBuffer(GL_ARRAY_BUFFER, VBO);
        glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices), vertices);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        // Render quad

        glm::mat4 model;

        model = glm::translate(model, glm::vec3(position.x, position.y, 0));
        model = glm::scale(model, glm::vec3(scale, 1.0f));
        model = glm::rotate(model, glm::radians(rotation), glm::vec3(0, 0, 1));

        s.SetMatrix4("model", model);

        glDrawArrays(GL_TRIANGLES, 0, 6);

        float BKX = position.x;
        float BKY = position.y;

        // Now advance cursors for next glyph (note that advance is number of 1/64 pixels)
        position.x += ((ch.Advance >> 6) * scale.x); // Bitshift by 6 to get value in pixels (2^6 = 64)

        // Need a way to advance cursor y for rotation effect
        position.y += rotation / 3.0f;
        position.x -= rotation / 8.0f;

        if(TEST)
        {
            //printf("BX: %f , AX: %f , DiffX: %f\n", BKX, position.x, position.x - BKX);
            //printf("BY: %f , AY: %f , DiffY: %f\n", BKY, position.y, position.y - BKY);
        }
    }
    TEST = false;
    glBindVertexArray(0);
    glBindTexture(GL_TEXTURE_2D, 0);
}
