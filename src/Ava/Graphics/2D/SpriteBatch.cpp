//
// Created by Keith Webb on 11/19/15.
//

#include <Ava/Graphics/2D/SpriteBatch.h>

SpriteBatch::SpriteBatch()
{
    vertices = 0;

}

SpriteBatch::~SpriteBatch()
{

}

void SpriteBatch::begin()
{
    ready = true;
}

void SpriteBatch::render(Sprite &sprite, Shader* shader)
{
    if(!ready)
        return;

    sprites.push_back(&sprite);
    shaders.push_back(shader);

    vertices += 6;
}

void SpriteBatch::end()
{
    ready = false;

    //Fake A SpriteBatch
    for(int i = 0; i < sprites.size(); i++)
    {
        sprites[i]->render(shaders[i]);
    }

    //Clear
    sprites.clear();
    shaders.clear();
    vertices = 0;
}
