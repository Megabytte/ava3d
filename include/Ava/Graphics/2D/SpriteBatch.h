//
// Created by Keith Webb on 11/19/15.
//

#pragma once

#include <vector>
#include <Ava/Graphics/2D/Sprite.h>
#include "Camera2D.h"
#include <Ava/Util/Pool.h>

class SpriteBatch
{
public:
    SpriteBatch();
    ~SpriteBatch();

    void begin();
    void render(Sprite& sprite, Shader* shader);
    void end();

private:
    bool ready = false;
    std::vector<Sprite*> sprites;
    std::vector<Shader*> shaders;
    int vertices;
};
