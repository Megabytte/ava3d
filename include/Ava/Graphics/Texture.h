//
// Created by Keith Webb on 11/18/15.
//

#pragma once

#include <string>
#include <GL/glew.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

class Texture {
public:
    Texture();
    Texture(unsigned int width, unsigned int height);
    Texture(std::string);
    ~Texture();

    void create(unsigned int width, unsigned int height);
    void loadTexture(std::string);

    void Bind() const;
    void Unbind() const;

    bool IsReady;

    // Holds the ID of the texture object, used for all texture operations to reference to this particlar texture
    GLuint ID;
    // Texture image dimensions
    GLuint Width, Height; // Width and Height of loaded image in pixels
    // Texture Format
    GLuint Internal_Format; // Format of texture object
    GLuint Image_Format; // Format of loaded image
    // Texture configuration
    GLuint Wrap_S; // Wrapping mode on S axis
    GLuint Wrap_T; // Wrapping mode on T axis
    GLuint Filter_Min; // Filtering mode if texture pixels < screen pixels
    GLuint Filter_Max; // Filtering mode if texture pixels > screen pixels
};