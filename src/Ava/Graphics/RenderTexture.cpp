//
// Created by Keith Webb on 11/29/15.
//

#include <Ava/Graphics/RenderTexture.h>
#include <Ava/Graphics/Display.h>

RenderTexture::RenderTexture()
{

}

RenderTexture::RenderTexture(unsigned int width, unsigned int height)
{
    create(width, height);
}

RenderTexture::~RenderTexture()
{
    glDeleteFramebuffers(1, &framebuffer);
    glDeleteRenderbuffers(1, &rbo);
}

void RenderTexture::create(unsigned int width, unsigned int height)
{
    texture.create(width, height);
    mWidth = width; mHeight = height;
    canRender = false;

    glGenFramebuffers(1, &framebuffer);
    glBindFramebuffer(GL_FRAMEBUFFER, framebuffer);

    // Attach it to currently bound framebuffer object
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texture.ID, 0);

    glGenRenderbuffers(1, &rbo);
    glBindRenderbuffer(GL_RENDERBUFFER, rbo);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, mWidth, mHeight);
    glBindRenderbuffer(GL_RENDERBUFFER, 0);

    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, rbo);

    if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        printf("ERROR::FRAMEBUFFER:: Framebuffer is not complete!\n");
    else
        canRender = true;

    this->clear();

    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void RenderTexture::renderTo(Drawable &drawable, Shader *shader)
{
    if(!canRender)
        return;

    glBindFramebuffer(GL_FRAMEBUFFER, framebuffer);
    glEnable(GL_CULL_FACE);
    glCullFace(GL_BACK);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_DEPTH_TEST);
    //glViewport(0, 0, mWidth, mHeight);

    drawable.render(shader);

    //glViewport(0, 0, Display::Width, Display::Height);
    glDisable(GL_CULL_FACE);
    glDisable(GL_BLEND);
    glDisable(GL_DEPTH_TEST);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void RenderTexture::clear(float r, float g, float b, float a)
{
    if(!canRender)
        return;

    glBindFramebuffer(GL_FRAMEBUFFER, framebuffer);
    glClearColor(r, g, b, a);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

