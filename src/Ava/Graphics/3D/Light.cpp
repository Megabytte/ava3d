//
// Created by Keith Webb on 11/26/15.
//

#include <Ava/Graphics/3D/Light.h>
#include <Ava/Util/ResourceManager.h>

int Light::MAX_LIGHTS = 10;
int Light::USED_LIGHTS = 0;
std::vector<Light*> Light::lights = {nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr};

Light::Light(LightType type)
{
    setColorGL(1.0f, 1.0f, 1.0f, 1.0f);
    if(USED_LIGHTS <= MAX_LIGHTS) {
        lightNum = 0;

        for(int i = 0; i < lights.size(); i++)
        {
            if(lights[i] == nullptr) {
                lightNum = i;
                lights[i] = this;
                break;
            }
        }

        USED_LIGHTS++;
        id = "lights[" + std::to_string(lightNum) + "]";
    }
    else
    {
        throw std::runtime_error("Maximum Number of Lights Exceeded!!");
    }

    mType = type;

    setup();

    constant = 1.0f;
    linear = 0.09f;
    quadratic = 0.032f;
    cutoff = 12.5f;
    outercutoff = 17.5f;
    direction.x = direction.y = direction.z = 0;
}

void Light::setup()
{
    GLfloat vertices[] = {
            -0.5f, -0.5f, -0.5f,
            0.5f, -0.5f, -0.5f,
            0.5f,  0.5f, -0.5f,
            0.5f,  0.5f, -0.5f,
            -0.5f,  0.5f, -0.5f,
            -0.5f, -0.5f, -0.5f,

            -0.5f, -0.5f,  0.5f,
            0.5f, -0.5f,  0.5f,
            0.5f,  0.5f,  0.5f,
            0.5f,  0.5f,  0.5f,
            -0.5f,  0.5f,  0.5f,
            -0.5f, -0.5f,  0.5f,

            -0.5f,  0.5f,  0.5f,
            -0.5f,  0.5f, -0.5f,
            -0.5f, -0.5f, -0.5f,
            -0.5f, -0.5f, -0.5f,
            -0.5f, -0.5f,  0.5f,
            -0.5f,  0.5f,  0.5f,

            0.5f,  0.5f,  0.5f,
            0.5f,  0.5f, -0.5f,
            0.5f, -0.5f, -0.5f,
            0.5f, -0.5f, -0.5f,
            0.5f, -0.5f,  0.5f,
            0.5f,  0.5f,  0.5f,

            -0.5f, -0.5f, -0.5f,
            0.5f, -0.5f, -0.5f,
            0.5f, -0.5f,  0.5f,
            0.5f, -0.5f,  0.5f,
            -0.5f, -0.5f,  0.5f,
            -0.5f, -0.5f, -0.5f,

            -0.5f,  0.5f, -0.5f,
            0.5f,  0.5f, -0.5f,
            0.5f,  0.5f,  0.5f,
            0.5f,  0.5f,  0.5f,
            -0.5f,  0.5f,  0.5f,
            -0.5f,  0.5f, -0.5f,
    };

    GLuint VBO, VAO;
    glGenVertexArrays(1, &VAO);
    glGenBuffers(1, &VBO);

    glBindVertexArray(VAO);

    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    // Position attribute
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (GLvoid*)0);
    glEnableVertexAttribArray(0);

    glBindVertexArray(0); // Unbind VAO
    this->VAO = VAO;
    this->VBO = VBO;
}

Light::~Light()
{
    USED_LIGHTS--;

    lights[lightNum] = nullptr;

    glDeleteVertexArrays(1, &VAO);
    glDeleteBuffers(1, &VBO);
}

void Light::render(Shader* shader)
{
    glDisable(GL_CULL_FACE);
    shader->Use();
    shader->SetInteger("numberOfLights", USED_LIGHTS);
    shader->SetVector3f(getCommand(".ambient").c_str(), 0.1f, 0.1f, 0.1f);
    shader->SetVector3f(getCommand(".diffuse").c_str(), color.r, color.g, color.b);
    shader->SetVector3f(getCommand(".specular").c_str(), 1.0f, 1.0f, 1.0f);
    shader->SetVector3f(getCommand(".lightVector").c_str(), position);
    if(mType == SPOT || mType == POINT) {
        shader->SetFloat(getCommand(".constant").c_str(), constant);
        shader->SetFloat(getCommand(".linear").c_str(), linear);
        shader->SetFloat(getCommand(".quadratic").c_str(), quadratic);
        if(mType == SPOT) {
            shader->SetBool(getCommand(".isPointLight").c_str(), (GLboolean) false);
            shader->SetBool(getCommand(".isSpotLight").c_str(), (GLboolean) true);
            shader->SetBool(getCommand(".isDirectionalLight").c_str(), (GLboolean) false);
            shader->SetVector3f(getCommand(".direction").c_str(), direction);
            shader->SetFloat(getCommand(".cutOff").c_str(), glm::cos(glm::radians(cutoff)));
            shader->SetFloat(getCommand(".outerCutOff").c_str(), glm::cos(glm::radians(outercutoff)));
        }
        else {
            shader->SetBool(getCommand(".isPointLight").c_str(), (GLboolean) true);
            shader->SetBool(getCommand(".isSpotLight").c_str(), (GLboolean) false);
            shader->SetBool(getCommand(".isDirectionalLight").c_str(), (GLboolean) false);
        }
    }
    else {
        shader->SetBool(getCommand(".isPointLight").c_str(), (GLboolean) false);
        shader->SetBool(getCommand(".isSpotLight").c_str(), (GLboolean) false);
        shader->SetBool(getCommand(".isDirectionalLight").c_str(), (GLboolean) true);
    }

    shader->SetBool("isLight", (GLboolean) true);

    if(mType != DIRECTIONAL) {
        glBindVertexArray(VAO);

        shader->SetMatrix4("model", getTransform());

        glDrawArrays(GL_TRIANGLES, 0, 36);

        glBindVertexArray(0);
    }

    glEnable(GL_CULL_FACE);
}

void Light::setLightType(Light::LightType type) {
    mType = type;
}
Light::LightType Light::getLightType() {
    return mType;
}
float Light::getConstant() {
    return constant;
}
void Light::setConstant(float constant) {
    this->constant = constant;
}
float Light::getLinear() {
    return linear;
}
void Light::setLinear(float linear) {
    this->linear = linear;
}
float Light::getQuadratic() {
    return quadratic;
}
void Light::setQuadratic(float quadratic) {
    this->quadratic = quadratic;
}
void Light::setDirectionX(float dirX) {
    direction.x = dirX;
}
void Light::setDirectionY(float dirY) {
    direction.y = dirY;
}
void Light::setDirectionZ(float dirZ) {
    direction.z = dirZ;
}
void Light::setDirection(glm::vec3 dir) {
    direction = dir;
}
void Light::setDirection(float dirX, float dirY, float dirZ) {
    direction.x = dirX;
    direction.y = dirY;
    direction.z = dirZ;
}
glm::vec3 Light::getDirection()
{
    return direction;
}

float Light::getCutOff(){ return cutoff; }
float Light::getOuterCutOff(){ return outercutoff; }
void Light::setCutOff(float v) { cutoff = v; }
void Light::setOuterCutOff(float v) { outercutoff = v; }

std::string Light::getCommand(std::string command)
{
    return (id + command);
}
