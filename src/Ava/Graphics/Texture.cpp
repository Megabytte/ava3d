//
// Created by Keith Webb on 11/18/15.
//

#include <Ava/Graphics/Texture.h>

Texture::Texture() : Width(0), Height(0), Internal_Format(GL_RGBA), Image_Format(GL_RGBA),
                     Wrap_S(GL_CLAMP_TO_EDGE), Wrap_T(GL_CLAMP_TO_EDGE), Filter_Min(GL_LINEAR), Filter_Max(GL_LINEAR), IsReady(false)
{

}

Texture::Texture(unsigned int width, unsigned int height) : Width(width), Height(height), Internal_Format(GL_RGBA), Image_Format(GL_RGBA),
                     Wrap_S(GL_CLAMP_TO_EDGE), Wrap_T(GL_CLAMP_TO_EDGE), Filter_Min(GL_LINEAR), Filter_Max(GL_LINEAR), IsReady(false)
{
    create(width, height);
}

void Texture::create(unsigned int width, unsigned int height)
{
    if(IsReady)
    {
        glDeleteTextures(1, &ID);
    }

    Width = width; Height = height;

    glGenTextures(1, &ID);
    glBindTexture(GL_TEXTURE_2D, ID);

    glTexImage2D(GL_TEXTURE_2D, 0, this->Internal_Format, Width, Height, 0, this->Image_Format, GL_UNSIGNED_BYTE, NULL);
    glGenerateMipmap(GL_TEXTURE_2D);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, this->Wrap_S);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, this->Wrap_T);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, this->Filter_Min);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, this->Filter_Max);

    glBindTexture(GL_TEXTURE_2D, 0);

    IsReady = true;
}

Texture::Texture(std::string path) : Width(0), Height(0), Internal_Format(GL_RGB), Image_Format(GL_RGB),
                                     Wrap_S(GL_REPEAT), Wrap_T(GL_REPEAT), Filter_Min(GL_LINEAR), Filter_Max(GL_LINEAR), IsReady(false)
{
    loadTexture(path);
}

Texture::~Texture()
{
    glDeleteTextures(1, &ID);
}

void Texture::loadTexture(std::string path)
{
    GLuint TextureID = 0;

    SDL_Surface* Surface = IMG_Load(path.c_str());

    glGenTextures(1, &TextureID);
    glBindTexture(GL_TEXTURE_2D, TextureID);

    if(Surface->format->BytesPerPixel == 4) {
        Internal_Format = GL_RGBA;
        Image_Format = GL_RGBA;
    }

    Width = (GLuint) Surface->w;
    Height = (GLuint) Surface->h;

    glTexImage2D(GL_TEXTURE_2D, 0, this->Internal_Format, Width, Height, 0, this->Image_Format, GL_UNSIGNED_BYTE, Surface->pixels);
    glGenerateMipmap(GL_TEXTURE_2D);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, this->Wrap_S);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, this->Wrap_T);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, this->Filter_Min);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, this->Filter_Max);

    glBindTexture(GL_TEXTURE_2D, 0);

    SDL_FreeSurface(Surface);

    ID = TextureID;
    IsReady = true;
}

void Texture::Bind() const
{
    glBindTexture(GL_TEXTURE_2D, this->ID);
}

void Texture::Unbind() const
{
    glBindTexture(GL_TEXTURE_2D, 0);
}
