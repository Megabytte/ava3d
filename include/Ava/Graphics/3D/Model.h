//
// Created by Keith Webb on 11/28/15.
//

#pragma once

#include <GL/glew.h>
#include <Ava/Graphics/Shader.h>
#include <Ava/Graphics/3D/Mesh.h>

#include <string>
#include <vector>

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include <Ava/Graphics/3D/Transformable3D.h>

class Model : public Transformable3D
{
public:
    /*  Functions   */
    Model(std::string path);
    void Draw(Shader* shader);
private:
    GLuint TextureFromFile(aiString file, std::string dir);

    /*  Model Data  */
    std::vector<Mesh> meshes;
    std::vector<Mesh::Texture> textures_loaded;
    std::string directory;
    /*  Functions   */
    void loadModel(std::string path);
    void processNode(aiNode* node, const aiScene* scene);
    Mesh processMesh(aiMesh* mesh, const aiScene* scene);
    std::vector<Mesh::Texture> loadMaterialTextures(aiMaterial* mat, aiTextureType type, std::string typeName);
};
