//
// Created by Keith Webb on 11/24/15.
//

#pragma once

template <class T>
class Rectangle
{
public:
    Rectangle() { x = y = width = height = 0; }
    Rectangle(T lX, T lY, T lWidth, T lHeight) : x(lX), y(lY), width(lWidth), height(lHeight) { }

    ~Rectangle() { x = y = width = height = 0; }

    void set(T lX, T lY, T lWidth, T lHeight)
    { x = lX; y = lY; width = lWidth; height = lHeight; }

    void setX(T lX) { x = lX; }
    void setY(T lY) { y = lY; }
    void setWidth(T lWidth) { width = lWidth; }
    void setHeight(T lHeight) { height = lHeight; }

    T getX() { return x; }
    T getY() { return y; }
    T getWidth() { return width; }
    T getHeight() { return height; }

    bool intersect(Rectangle* other)
    {
        bool r1 = (this->getX() < other->getX() + other->getWidth());
        bool r2 = (this->getX() + this->getWidth() < other->getX());
        bool r3 = (this->getY() < other->getY() + other->getHeight());
        bool r4 = (this->getY() + this->getHeight() < other->getY());

        return (r1 || r2 || r3 || r4);
    }

    bool contains(Rectangle* other)
    {
        bool r1 = (this->getX() <= other->getX());
        bool r2 = (this->getY() <= other->getY());

        bool r3 = (this->getX() + this->getWidth() >= other->getX() + other->getWidth());
        bool r4 = (this->getY() + this->getHeight() >= other->getY() + other->getHeight());

        return (r1 && r2 && r3 && r4);
    }

private:
    T x, y, width, height;
};
