#
# this module look for SDL2_image (http://www.libsdl.org) support
# it will define the following values
#
# SDLIMAGE_INCLUDE_DIR  = where SDL_image.h can be found
# SDLIMAGE_LIBRARY      = the library to link against SDL2_image
# SDLIMAGE_FOUND        = set to 1 if SDL2_image is found
#

IF(SDL2_image_INCLUDE_DIRS)

  FIND_PATH(SDLIMAGE_INCLUDE_DIR SDL2/SDL_image.h ${SDL2_image_INCLUDE_DIRS})
  FIND_LIBRARY(SDLIMAGE_LIBRARY SDL2_image ${SDL2_image_LIBRARY_DIRS})

ELSE(SDL2_image_INCLUDE_DIRS)

  SET(TRIAL_LIBRARY_PATHS
          $ENV{SDL2_image_HOME}/lib
          /usr/lib
          /usr/local/lib
          /sw/lib
          )
  SET(TRIAL_INCLUDE_PATHS
          $ENV{SDL2_image_HOME}/include/SDL2
          /usr/include/SDL2
          /usr/local/include/SDL2
          /sw/include/SDL2
          )

  FIND_LIBRARY(SDLIMAGE_LIBRARY SDL2_image ${TRIAL_LIBRARY_PATHS})
  FIND_PATH(SDLIMAGE_INCLUDE_DIR SDL_image.h ${TRIAL_INCLUDE_PATHS})

ENDIF(SDL2_image_INCLUDE_DIRS)

IF(SDLIMAGE_INCLUDE_DIR AND SDLIMAGE_LIBRARY)
  SET(SDLIMAGE_FOUND 1 CACHE BOOL "Found SDL2_image library")
ELSE(SDLIMAGE_INCLUDE_DIR AND SDLIMAGE_LIBRARY)
  SET(SDLIMAGE_FOUND 0 CACHE BOOL "Not fount SDL2_image library")
ENDIF(SDLIMAGE_INCLUDE_DIR AND SDLIMAGE_LIBRARY)

MARK_AS_ADVANCED(
        SDLIMAGE_INCLUDE_DIR
        SDLIMAGE_LIBRARY
        SDLIMAGE_FOUND
)
