//
// Created by Keith Webb on 11/18/15.
//

#include <Ava/Graphics/Shader.h>
#include <Ava/Graphics/2D/Camera2D.h>
#include <Ava/Graphics/Display.h>

Camera2D::Camera2D()
{
    position.x = 0;
    position.y = 0;
    scale.x = scale.y = 1.0f;
    width = Display::Width;
    height = Display::Height;

    glm::vec3 cameraPos = glm::vec3(position, 0.0f);
    glm::vec3 cameraFront = glm::vec3(0.0f, 0.0f, -1.0f);
    glm::vec3 cameraUp = glm::vec3(0.0f, 1.0f,  0.0f);

    view = glm::lookAt(cameraPos, cameraPos + cameraFront, cameraUp);
}

Camera2D::Camera2D(float x, float y, float width, float height)
{
    this->position.x = x;
    this->position.y = y;
    this->width = width;
    this->height = height;
    scale.x = scale.y = 1.0f;

    glm::vec3 cameraPos = glm::vec3(x, y, 0.0f);
    glm::vec3 cameraFront = glm::vec3(0.0f, 0.0f, -1.0f);
    glm::vec3 cameraUp = glm::vec3(0.0f, 1.0f,  0.0f);

    view = glm::lookAt(cameraPos, cameraPos + cameraFront, cameraUp);
}

void Camera2D::update()
{
    glm::vec3 cameraPos = glm::vec3(position, 0.0f);
    glm::vec3 cameraFront = glm::vec3(0.0f, 0.0f, -1.0f);
    glm::vec3 cameraUp = glm::vec3(0.0f, 1.0f,  0.0f);

    view = glm::lookAt(cameraPos, cameraPos + cameraFront, cameraUp);

    view = glm::translate(view, glm::vec3(0.5f * width, 0.5f * height, 0.0f));
    view = glm::rotate(view, glm::radians(rotation), glm::vec3(0.0f, 0.0f, 1.0f));
    view = glm::scale(view, glm::vec3(scale, 1.0f));
    view = glm::translate(view, glm::vec3(-0.5f * width, -0.5f * height, 0.0f));
}

void Camera2D::apply(Shader *shader)
{
    shader->Use();
    shader->SetMatrix4("view", view);
    shader->SetMatrix4("projection", Shader::Global_Orthographic_Projection);
}
