//
// Created by Keith Webb on 11/29/15.
//

#pragma once

#include <string>
#include <freetype/ft2build.h>
#include <GL/glew.h>
#include <glm/detail/type_vec.hpp>
#include <glm/detail/type_vec2.hpp>
#include <map>
#include <Ava/Graphics/Shader.h>
#include FT_FREETYPE_H

class Font
{
public:
    Font();
    Font(std::string path, unsigned int size=32);
    ~Font();

    void loadFromFile(std::string path, unsigned int size=32);

    void render(Shader &s, std::string text, glm::vec2 position, float rotation, glm::vec2 scale, glm::vec2 center, glm::vec4 color);

    struct Character {
        GLuint TextureID;  // ID handle of the glyph texture
        glm::ivec2 Size;       // Size of glyph
        glm::ivec2 Bearing;    // Offset from baseline to left/top of glyph
        GLuint Advance;    // Offset to advance to next glyph
    };

    unsigned int getFontSize() { return fontSize; }

private:
    std::map<GLchar, Character> Characters;
    FT_Face face;
    bool canRender;
    unsigned int fontSize;
    GLuint VAO, VBO;

    bool TEST = true;
};
