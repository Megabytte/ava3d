//
// Created by Keith Webb on 11/19/15.
//

#pragma once

#include <SDL2/SDL.h>
#include <string>

class Display
{
public:
    Display(int width, int height, std::string title);
    ~Display();

    void clear(float r = 0.0f, float g = 0.0f, float b = 0.0f, float a = 1.0f);

    void flip();

    static int Width;
    static int Height;

private:
    SDL_Window* window;
    SDL_GLContext context;
};
