# README #

This is my 3D game engine I worked on to learn OpenGL and some 3D math. It is incomplete, probably has bugs, and isn't recommended for serious use. However, it is available under the zlib/libpng license. You can use what I have here to start your own project. 

### Dependencies ###
1. OpenGL
2. GLEW
3. SDL2
4. SDL2 Image
5. SDL2_net
6. FMOD
7. AssImp
8. Freetype

### To Build ###
1. Install the Dependencies
2. Use CMake to generate a project
3. Use the generated project to build and edit Ava3D

### License ###

Copyright (c) 2016 Keith Webb

This software is provided 'as-is', without any express or implied warranty. In no event will the authors be held liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose, including commercial applications, and to alter it and redistribute it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim that you wrote the original software. If you use this software in a product, an acknowledgment in the product documentation would be appreciated but is not required.

2. Altered source versions must be plainly marked as such, and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.