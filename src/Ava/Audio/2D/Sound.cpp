//
// Created by Keith Webb on 11/23/15.
//

#include <Ava/Audio/2D/Sound.h>
#include <Ava/Util/System.h>

Sound::Sound()
{
    fmod_channel = nullptr;
    fmod_handle = nullptr;
    ready = false;
}

Sound::Sound(std::string path)
{
    loadSound(path);
}

Sound::~Sound()
{
    fmod_channel->stop();
    fmod_handle->release();

    fmod_channel = nullptr;
    fmod_handle = nullptr;
    ready = false;
}

void Sound::loadSound(std::string path)
{
    System::FMOD_System->createSound(path.c_str(), FMOD_DEFAULT, 0, &fmod_handle);
    ready = true;
}

void Sound::play(int repeats)
{
    if(ready) {
        fmod_handle->setLoopCount(repeats);
        System::FMOD_System->playSound(fmod_handle, 0, false, &fmod_channel);
    }
}

void Sound::pause()
{
    if(ready) {
        fmod_channel->setPaused(true);
    }
}

void Sound::resume()
{
    if(ready) {
        fmod_channel->setPaused(false);
    }
}

void Sound::stop()
{
    if(ready) {
        fmod_channel->stop();
    }
}

bool Sound::IsMusicPlaying()
{
    if(ready) {
        bool r = false;
        fmod_channel->isPlaying(&r);
        return r;
    } else {
        return false;
    }
}

bool Sound::IsMusicPaused() {
    if(ready) {
        bool r = false;
        fmod_channel->getPaused(&r);
        return r;
    } else {
        return false;
    }
}
