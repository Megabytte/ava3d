//
// Created by Keith Webb on 11/28/15.
//

#pragma once

#include <glm/detail/type_vec.hpp>
#include <glm/detail/type_vec2.hpp>
#include <glm/detail/type_vec3.hpp>

#include <GL/glew.h>
#include <string>
#include <vector>
#include <Ava/Graphics/Shader.h>
#include <assimp/types.h>

class Model;

class Mesh
{
public:
    struct Vertex {
        glm::vec3 Position;
        glm::vec3 Normal;
        glm::vec2 TexCoords;
    };

    struct Texture {
        GLuint id;
        std::string type;
        aiString path;
    };

    std::vector<Vertex> vertices;
    std::vector<GLuint> indices;
    std::vector<Texture> textures;
    /*  Functions  */
    Mesh(std::vector<Vertex> vertices, std::vector<GLuint> indices, std::vector<Texture> textures, Model* parent);
    void Draw(Shader* shader);

private:
    Model* parent;
    /*  Render data  */
    GLuint VAO, VBO, EBO;
    /*  Functions    */
    void setupMesh();
};
