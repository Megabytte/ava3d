//
// Created by Keith Webb on 11/29/15.
//

#pragma once

#include <GL/glew.h>
#include <Ava/Graphics/Drawable.h>
#include <Ava/Graphics/Texture.h>
#include <Ava/Graphics/2D/Sprite.h>

class RenderTexture
{
public:
    RenderTexture();
    RenderTexture(unsigned int width, unsigned int height);
    ~RenderTexture();

    void create(unsigned int width, unsigned int height);

    void renderTo(Drawable& drawable, Shader* shader);

    Texture* getTexture() { return &texture; }

    void clear(float r = 0.0f, float g = 0.0f, float b = 0.0f, float a = 1.0f);

private:
    Texture texture;
    GLuint framebuffer, rbo;
    int mWidth, mHeight;
    bool canRender;
};
