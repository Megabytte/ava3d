//
// Created by Keith Webb on 11/28/15.
//

#include <Ava/Util/System.h>
#include <MainState.h>
#include "FlatState.h"


FlatState::FlatState()
{

}

FlatState::~FlatState()
{
    InputManager::RemoveReciever(this);
    ResourceManager::FreeShader("2D");
    ResourceManager::FreeTexture("container");
}

void FlatState::create()
{
    InputManager::AddReciever(this);
    shader2D = ResourceManager::LoadShader("2D", "shaders/2d.vert", "shaders/2d.frag");
    sprite.setTexture(ResourceManager::LoadTexture("container", "textures/container2.png"));
    System::ShowMouse();
}

void FlatState::update(float dt)
{
    camera.update();
}

void FlatState::render()
{
    game->display.clear();

    camera.apply(shader2D);

    sprite.render(shader2D);

    game->display.flip();
}

void FlatState::event(std::string eventType, std::string eventValue)
{
    if(eventType == "KeyDown")
    {
        if(eventValue == "1")
        {
            GameStateManager::ChangeState(new MainState());
        }
    }
}
