//
// Created by Keith Webb on 11/22/15.
//

#pragma once

#include <type_traits>
#include <string>
#include <cstdarg>
#include <vector>
#include <functional>

class Poolable
{
public:
    virtual bool inUse() { return inuse; }
    void setInUse(bool val) { inuse = val; }
    virtual void create(int size, va_list list) = 0;

    bool inuse = true;
    std::string name = "";
    int priority = 0;
};

enum OutOfSpaceBehavior {IGNORE, PREVENT, KILL_EXISTING, RESIZE};

template <class T>
class Pool
{
    static_assert(
            std::is_base_of<Poolable, T>::value,
            "Generic must be a descendant of Poolable"
    );

public:
    Pool(int maxSize, OutOfSpaceBehavior behavior);
    ~Pool();

    T* create(int variableCount, ...);
    void add(T& value);
    void remove(T& value);
    void remove(std::string name);

    template<typename E, typename RT, typename ... Params>
    std::vector<RT> callToAllWithReturn(E e, Params ... p);
    template<typename E, typename ... Params>
    void callToAll(E e, Params ... p);

    std::vector<T*> getContents() { return array; }
    int getMaxSize() { return maxSize; }
    int getCurrentSize() { return (int) array.size(); }
    OutOfSpaceBehavior getBehavior() { return behavior; }
    void setNewBehavior(OutOfSpaceBehavior b) { behavior = b; }

private:
    std::vector<T*> array;
    int maxSize;
    OutOfSpaceBehavior behavior;
};

template <class T>
Pool<T>::Pool(int maxSize, OutOfSpaceBehavior behavior)
{
    this->maxSize = maxSize;
    this->behavior = behavior;
}

template <class T>
Pool<T>::~Pool()
{
    for(int i = 0; i < maxSize; i++)
    {
        if(this->array[i] != nullptr)
        {
            delete this->array[i];
        }
    }

    array.clear();
}

template <class T>
T* Pool<T>::create(int count, ...)
{
    if(array.size() >= maxSize)
    {
        switch(behavior)
        {
            //Do Nothing
            case IGNORE:
            {
                return nullptr;
            }
            //Kill First Not In Use or Do Nothing
            case PREVENT: {
                for (int i = 0; i < array.size(); i++) {
                    if (!array[i]->inUse()) {
                        va_list vl;
                        va_start(vl, count);
                        array[i]->setInUse(true);
                        array[i]->create(count, vl);
                        va_end(vl);
                        return array[i];
                    }
                }
                return nullptr;
            }
            //Kill First Not In Use or First Low Priority
            case KILL_EXISTING:
            {
                int position = -1;
                int p = array[0]->priority;
                for (int i = 0; i < array.size(); i++)
                {
                    if(!array[i]->inUse())
                    {
                        position = i;
                        break;
                    }
                    if (array[i]->priority <= p)
                    {
                        position = i;
                        p = array[i]->priority;
                    }
                }

                if(position == -1)
                    position = (int) (array.size() - 1);

                va_list vl;
                va_start(vl, count);
                array[position]->create(count, vl);
                va_end(vl);

                return array[position];
            }
            //Double Size
            case RESIZE:
            {
                maxSize *= 2;
            }
                break;
        }
    }

    array.push_back(new T());

    va_list vl;
    va_start(vl, count);
    array.back()->create(count, vl);
    va_end(vl);

    return array.back();
}

template <class T>
void Pool<T>::add(T &value)
{
    if(array.size() >= maxSize)
    {
        switch(behavior)
        {
            //Do Nothing
            case IGNORE:
            {
                return;
            }
                //Kill First Not In Use or Do Nothing
            case PREVENT:
            {
                for (int i = 0; i < array.size(); i++) {
                    if (!array[i]->inUse()) {
                        delete array[i];
                        array[i] = value;
                        return;
                    }
                }
                return;
            }
                //Kill First Not In Use or First Low Priority
            case KILL_EXISTING:
            {
                int position = -1;
                int p = array[0]->priority;
                for (int i = 0; i < array.size(); i++)
                {
                    if(!array[i]->inUse())
                    {
                        position = i;
                        break;
                    }
                    if (array[i]->priority <= p)
                    {
                        position = i;
                        p = array[i]->priority;
                    }
                }

                if(position == -1)
                    position = (int) (array.size() - 1);

                delete array[position];
                array[position] = value;

                return;
            }
                //Double Size
            case RESIZE:
            {
                maxSize *= 2;
            }
                break;
        }
    }

    array.push_back(value);
}

template <class T>
void Pool<T>::remove(T &value)
{
    for(int i = 0; i < array.size(); i++)
    {
        if(value == array[i])
        {
            array[i]->setInUse(false);
        }
    }
}

template <class T>
void Pool<T>::remove(std::string name)
{
    for(int i = 0; i < array.size(); i++)
    {
        if(array[i] != nullptr)
        {
            if(array[i]->name == name)
            {
                array[i]->setInUse(false);
            }
        }
    }
}

template <class T>
template<typename E, typename RT, typename ... Params>
std::vector<RT> Pool<T>::callToAllWithReturn(E e, Params ... p)
{
    std::vector<RT> returns;
    for(auto val : array)
    {
        returns.push_back(e(val, p...));
    }

    return returns;
}

template <class T>
template<typename E, typename ... Params>
void Pool<T>::callToAll(E e, Params ... p)
{
    for(auto val : array)
    {
        e(val, p...);
    }
}
