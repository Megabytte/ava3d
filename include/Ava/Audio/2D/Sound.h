//
// Created by Keith Webb on 11/23/15.
//

#pragma once

#include <string>
#include <fmodex/fmod.hpp>

class Sound
{
public:
    Sound();
    Sound(std::string path);
    ~Sound();

    void loadSound(std::string path);

    void play(int repeats = 0);
    void pause();
    void resume();
    void stop();

    bool IsMusicPlaying();
    bool IsMusicPaused();

private:
    bool ready;
    FMOD::Sound *fmod_handle;
    FMOD::Channel* fmod_channel;
};
