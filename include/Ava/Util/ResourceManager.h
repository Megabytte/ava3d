//
// Created by Keith Webb on 11/18/15.
//

#pragma once

#include <map>
#include <Ava/Graphics/Texture.h>
#include <Ava/Graphics/Shader.h>
#include <Ava/Audio/2D/Music.h>
#include <Ava/Audio/2D/Sound.h>
#include <Ava/Graphics/2D/Font.h>

class ResourceManager
{
public:
    static Texture* LoadTexture(std::string name, std::string path);
    static Texture* GetTexture(std::string name);
    static void FreeTexture(std::string name);

    static Music* LoadMusic(std::string name, std::string path);
    static Music* GetMusic(std::string name);
    static void FreeMusic(std::string name);

    static Sound* LoadSound(std::string name, std::string path);
    static Sound* GetSound(std::string name);
    static void FreeSound(std::string name);

    static Font* LoadFont(std::string name, std::string path);
    static Font* GetFont(std::string name);
    static void FreeFont(std::string name);

    static Shader* LoadShader(std::string name, std::string vertexPath, std::string fragmentPath);
    static Shader* GetShader(std::string name);
    static void FreeShader(std::string name);

private:
    ResourceManager();
    ~ResourceManager();
    static std::map<std::string, Texture *> textureMap;
    static std::map<std::string, Shader*> shaderMap;
    static std::map<std::string, Sound *> soundEffectMap;
    static std::map<std::string, Music*> musicMap;
    static std::map<std::string, Font*> fontMap;
};
