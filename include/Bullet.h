//
// Created by Keith Webb on 11/22/15.
//

#pragma once

#include <Ava/Util/Pool.h>
#include <Ava/Graphics/2D/Sprite.h>
#include <Ava/Util/ResourceManager.h>

class Bullet : public Poolable, public Sprite
{
public:
    Bullet()
    {
        setTexture(ResourceManager::GetTexture("bullet"));
        name = "";
        priority = 0;
        setInUse(true);
    }
    Bullet(std::vector<void*> &args)
    {
        setPosition(*static_cast<float*>(args[0]), *static_cast<float*>(args[1]));
        setRotation(180);
        setInUse(true);
        setTexture(ResourceManager::GetTexture("bullet"));
        name = "";
        priority = 0;
    }

    virtual std::string GetType()
    {
        return "Bullet";
    }

    static void* CreateObj(std::vector<void*> &args)
    {
        int ArgCount = (int) args.size();
        if (ArgCount == 0)
            return new Bullet();
        else if (ArgCount != 2) //Num Args
            return nullptr;
        Bullet* pBullet = new Bullet(args);
        return pBullet;
    }

    virtual void create(int size, va_list list)
    {
        setInUse(true);

        setRotation(180);
        for (int i = 0; i < size; i++)
        {
            double val = va_arg(list, double);
            switch (i)
            {
                case 0: setPositionX((float) val);
                    break;
                case 1: setPositionY((float) val);
                    break;
                default:
                    break;
            }
        }
    }

    void update(float dt)
    {
        setPositionY(getPositionY() + 100 * (dt / 1000));

        if(getPositionY() > 600 || getPositionY() < 0)
            inuse = false;
    }

};
