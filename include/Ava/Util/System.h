//
// Created by Keith Webb on 11/23/15.
//

#pragma once

#include <fmodex/fmod.hpp>
#include <SDL2/SDL_image.h>

#include <freetype/ft2build.h>
#include FT_FREETYPE_H

class System
{
public:
    static void Init();
    static void DeInit();

    static void InitSDL();
    static void InitSDL_Image(int flags = IMG_INIT_JPG | IMG_INIT_PNG);
    static void InitSDL_Net();
    static void InitFMOD();
    static void InitFreetype();

    static void DeInitSDL();
    static void DeInitSDL_Image();
    static void DeInitSDL_Net();
    static void DeInitFMOD();
    static void DeInitFreetype();

    static void ShowMouse();
    static void HideMouse();
    static void SetMousePosition(int x, int y);

    static FMOD::System* FMOD_System;
    static FT_Library ft;
};