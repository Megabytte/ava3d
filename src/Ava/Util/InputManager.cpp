//
// Created by Keith Webb on 11/18/15.
//

#include <Ava/Util/InputManager.h>
#include <Ava/Util/EventReciever.h>
#include <glm/detail/type_vec2.hpp>
#include <iostream>

std::vector<EventReciever*> InputManager::recievers;
std::unordered_map<std::string, bool> InputManager::keys;
bool InputManager::leftMB = false;
bool InputManager::middleMB = false;
bool InputManager::rightMB = false;

InputManager::InputManager()
{

}

InputManager::~InputManager()
{
    recievers.clear();
}

void InputManager::AddReciever(EventReciever *reciever)
{
    recievers.push_back(reciever);
}

void InputManager::RemoveReciever(EventReciever *reciever)
{
    auto it = recievers.begin();
    std::vector<std::vector<EventReciever*>::const_iterator> toRemove;
    for(; it != recievers.end(); ++it)
    {
        if(reciever == *it || *it == nullptr)
        {
            toRemove.push_back(it);
        }
    }

    for(auto i : toRemove)
    {
        recievers.erase(i);
    }
    toRemove.clear();
}

void InputManager::Event(SDL_Event event)
{
    if(event.type == SDL_KEYDOWN && event.key.repeat == 0)
    {
        const char* c = SDL_GetKeyName(event.key.keysym.sym);
        keys[c] = true;

        if(recievers.size() == 0)
            return;

        for(EventReciever* er : recievers)
        {
            if(er == nullptr)
            {
                RemoveReciever(er);
                continue;
            }
            er->event("KeyDown", c);
        }
    }
    else if(event.type == SDL_KEYUP)
    {
        const char* c = SDL_GetKeyName(event.key.keysym.sym);
        keys[c] = false;

        if(recievers.size() == 0)
            return;

        for(EventReciever* er : recievers)
        {
            if(er == nullptr)
            {
                RemoveReciever(er);
                continue;
            }
            er->event("KeyUp", c);
        }
    }
    else if(event.type == SDL_MOUSEBUTTONDOWN)
    {
        std::string c = "";
        switch(event.button.button)
        {
            case SDL_BUTTON_LEFT: leftMB = true; c = "Left";
                break;
            case SDL_BUTTON_MIDDLE: middleMB = true; c = "Middle";
                break;
            case SDL_BUTTON_RIGHT: rightMB = true; c = "Right";
                break;
            default:
                return;
        }

        if(recievers.size() == 0)
            return;

        for(EventReciever* er : recievers)
        {
            if(er == nullptr)
            {
                RemoveReciever(er);
                continue;
            }
            er->event("MouseDown", c);
        }
    }
    else if(event.type == SDL_MOUSEBUTTONUP)
    {
        std::string c = "";
        switch(event.button.button)
        {
            case SDL_BUTTON_LEFT: leftMB = false; c = "Left";
                break;
            case SDL_BUTTON_MIDDLE: middleMB = false; c = "Middle";
                break;
            case SDL_BUTTON_RIGHT: rightMB = false; c = "Right";
                break;
            default:
                return;
        }

        if(recievers.size() == 0)
            return;

        for(EventReciever* er : recievers)
        {
            if(er == nullptr)
            {
                RemoveReciever(er);
                continue;
            }
            er->event("MouseUp", c);
        }
    }
    else if(event.type == SDL_MOUSEWHEEL)
    {
        std::string c = std::to_string(event.wheel.y);

        if(recievers.size() == 0)
            return;

        for(EventReciever* er : recievers)
        {
            if(er == nullptr)
            {
                RemoveReciever(er);
                continue;
            }
            er->event("MouseWheel", c);
        }
    }
}

bool InputManager::IsKeyDown(std::string key)
{
    return keys[key];
}

bool InputManager::IsMBDown(std::string mb)
{
    if(mb == "Left" || mb == "left" || mb == "1")
        return leftMB;
    else if(mb == "Middle" || mb == "middle" || mb == "2")
        return middleMB;
    else if(mb == "Right" || mb == "right" || mb == "3")
        return rightMB;

    return false;
}

glm::vec2 InputManager::GetMousePosition()
{
    int x = -1, y = -1;
    SDL_GetMouseState(&x, &y);
    return glm::vec2(x, y);
}

bool InputManager::remove(EventReciever *one, EventReciever *two)
{
    return (one == two);
}
