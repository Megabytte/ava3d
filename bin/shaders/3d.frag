#version 330 core

struct Material {
    sampler2D texture_diffuse1;
    sampler2D texture_specular1;
    sampler2D texture_emission1;
    float shininess;
};

struct Light {
    bool isDirectionalLight;
    bool isSpotLight;
    bool isPointLight;

    vec3 lightVector;

    vec3 direction;
    float cutOff;
    float outerCutOff;

    vec3 ambient;
    vec3 diffuse;
    vec3 specular;

    float constant;
    float linear;
    float quadratic;
};

out vec4 color;

in vec2 TexCoords;
in vec3 Normal;
in vec3 FragPos;

uniform vec3 viewPos;

uniform Material material;

#define NR_POINT_LIGHTS 10
uniform int numberOfLights;
uniform bool isLight;
uniform Light lights[NR_POINT_LIGHTS];

vec4 calculateSpotLight(Light l, Material m, vec2 TexC, vec3 normal, vec3 fragPos, vec3 viewP)
{
    // Ambient
    vec3 ambient = l.ambient * vec3(texture(m.texture_diffuse1, TexC));

    // Diffuse
    vec3 norm = normalize(normal);
    vec3 lightDir = normalize(l.lightVector - fragPos);
    float diff = max(dot(norm, lightDir), 0.0);
    vec3 diffuse = l.diffuse * diff * vec3(texture(m.texture_diffuse1, TexC));

    // Specular
    vec3 viewDir = normalize(viewP - fragPos);
    vec3 reflectDir = reflect(-lightDir, norm);
    float spec = pow(max(dot(viewDir, reflectDir), 0.0), m.shininess);
    vec3 specular = l.specular * spec * vec3(texture(m.texture_specular1, TexC));

    // Spotlight (soft edges)
    float theta = dot(lightDir, normalize(-l.direction));
    float epsilon = (l.cutOff - l.outerCutOff);
    float intensity = clamp((theta - l.outerCutOff) / epsilon, 0.0, 1.0);
    diffuse *= intensity;
    specular *= intensity;

    // Attenuation
    float distance = length(l.lightVector - fragPos);
    float attenuation = 1.0f / (l.constant + l.linear * distance + l.quadratic * (distance * distance));
    ambient *= attenuation;
    diffuse *= attenuation;
    specular *= attenuation;

    return vec4(ambient + diffuse + specular, 1.0f);
}
vec4 calculatePointLight(Light l, Material m, vec2 TexC, vec3 normal, vec3 fragPos, vec3 viewP)
{
    vec3 ambient = l.ambient * vec3(texture(m.texture_diffuse1, TexC));
    vec3 norm = normalize(normal);

    vec3 lightDir = normalize(l.lightVector - fragPos);
    float distance = length(l.lightVector - fragPos);
    float attenuation = 1.0f / (l.constant + l.linear * distance +
                    		    l.quadratic * (distance * distance));

    float diff = max(dot(norm, lightDir), 0.0);
    vec3 diffuse = l.diffuse * diff * vec3(texture(m.texture_diffuse1, TexC));

    // Specular
    vec3 viewDir = normalize(viewP - fragPos);
    vec3 reflectDir = reflect(-lightDir, norm);
    float spec = pow(max(dot(viewDir, reflectDir), 0.0), m.shininess);
    vec3 specular = l.specular * spec * vec3(texture(m.texture_specular1, TexC));

    ambient *= attenuation;
    diffuse *= attenuation;
    specular *= attenuation;

    vec3 emission = vec3(texture(m.texture_emission1, TexC));

    return vec4(ambient + diffuse + specular + emission, 1.0f);
}
vec4 calculateDirectionalLight(Light l, Material m, vec2 TexC, vec3 normal, vec3 fragPos, vec3 viewP)
{
    vec3 ambient = l.ambient * vec3(texture(m.texture_diffuse1, TexC));
    vec3 norm = normalize(normal);

    vec3 lightDir = normalize(-l.lightVector);

    float diff = max(dot(norm, lightDir), 0.0);
    vec3 diffuse = l.diffuse * diff * vec3(texture(m.texture_diffuse1, TexC));

    // Specular
    vec3 viewDir = normalize(viewP - fragPos);
    vec3 reflectDir = reflect(-lightDir, norm);
    float spec = pow(max(dot(viewDir, reflectDir), 0.0), m.shininess);
    vec3 specular = l.specular * spec * vec3(texture(m.texture_specular1, TexC));

    vec3 emission = vec3(texture(m.texture_emission1, TexC));

    return vec4(ambient + diffuse + specular + emission, 1.0f);
}

void main()
{
    if(isLight)
    {
        color = vec4(1.0f);
        return;
    }

    vec4 result = vec4(0, 0, 0, 1);

    for(int i = 0; i < numberOfLights; i++)
    {
        if(lights[i].isDirectionalLight)
            result += calculateDirectionalLight(lights[i], material, TexCoords, Normal, FragPos, viewPos);
        else if(lights[i].isPointLight)
            result += calculatePointLight(lights[i], material, TexCoords, Normal, FragPos, viewPos);
        else if(lights[i].isSpotLight)
            result += calculateSpotLight(lights[i], material, TexCoords, Normal, FragPos, viewPos);
    }

    color = result;
}
