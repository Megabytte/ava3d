//
// Created by Keith Webb on 11/18/15.
//

#pragma once

#include <glm/detail/type_mat.hpp>
#include <glm/detail/type_mat4x4.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <Ava/Graphics/2D/Transformable2D.h>

class Camera2D : public Transformable2D
{
public:
    Camera2D();
    Camera2D(float x, float y, float width, float height);

    void update();

    void apply(Shader* shader);

    glm::mat4 view;
private:
    float width, height;
};
