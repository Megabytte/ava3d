#include <Ava/Util/System.h>
#include <Ava/Game.h>
#include "MainState.h"

int main()
{
    System::Init();

    Game game(800, 600, "Ava Engine");

    game.start(new MainState(), false, 45.0f, 0.1f, 100.0f);

    System::DeInit();

    return 0;
}
