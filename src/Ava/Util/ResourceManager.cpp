//
// Created by Keith Webb on 11/18/15.
//

#include <Ava/Util/ResourceManager.h>

std::map<std::string, Texture *> ResourceManager::textureMap;
std::map<std::string, Shader*> ResourceManager::shaderMap;
std::map<std::string, Sound *> ResourceManager::soundEffectMap;
std::map<std::string, Music*> ResourceManager::musicMap;
std::map<std::string, Font*> ResourceManager::fontMap;

ResourceManager::ResourceManager()
{

}

ResourceManager::~ResourceManager()
{
    for(auto t : textureMap)
    {
        delete t.second;
    }
    textureMap.clear();

    for(auto s : shaderMap)
    {
        delete s.second;
    }
    shaderMap.clear();

    for(auto s : soundEffectMap)
    {
        delete s.second;
    }
    soundEffectMap.clear();

    for(auto s : musicMap)
    {
        delete s.second;
    }
    musicMap.clear();
}

Texture* ResourceManager::LoadTexture(std::string name, std::string path)
{
    if(textureMap.find(name) == textureMap.end())
    {
        Texture* tex = new Texture(path);
        textureMap.insert(std::make_pair(name, tex));
        return tex;
    }
    else
    {
        return GetTexture(name);
    }
}

Texture* ResourceManager::GetTexture(std::string name)
{
    if(textureMap.find(name) != textureMap.end())
    {
        return textureMap.find(name)->second;
    }
    else
    {
        return nullptr;
    }
}

Shader* ResourceManager::LoadShader(std::string name, std::string vpath, std::string fpath)
{
    if(shaderMap.find(name) == shaderMap.end())
    {
        Shader* shader = new Shader();
        shader->CompileFromFile(vpath.c_str(), fpath.c_str());
        shaderMap.insert(std::make_pair(name, shader));
        return shader;
    }
    else
    {
        return GetShader(name);
    }
}

Shader* ResourceManager::GetShader(std::string name)
{
    if(shaderMap.find(name) != shaderMap.end())
    {
        return shaderMap.find(name)->second;
    }
    else
    {
        return nullptr;
    }
}

void ResourceManager::FreeTexture(std::string name)
{
    auto find = textureMap.find(name);
    if(find != textureMap.end())
    {
        textureMap.erase(find);
    }
}

void ResourceManager::FreeShader(std::string name)
{
    auto find = shaderMap.find(name);
    if(find != shaderMap.end())
    {
        shaderMap.erase(find);
    }
}

Music* ResourceManager::LoadMusic(std::string name, std::string path)
{
    if(musicMap.find(name) == musicMap.end())
    {
        Music* music = new Music(path);
        musicMap.insert(std::make_pair(name, music));
        return music;
    }
    else
    {
        return GetMusic(name);
    }
}

Music *ResourceManager::GetMusic(std::string name)
{
    auto find = musicMap.find(name);
    if(find != musicMap.end())
    {
        return find->second;
    }

    return nullptr;
}

void ResourceManager::FreeMusic(std::string name)
{
    auto find = musicMap.find(name);
    if(find != musicMap.end())
    {
        musicMap.erase(find);
    }
}

Sound* ResourceManager::LoadSound(std::string name, std::string path)
{
    if(soundEffectMap.find(name) == soundEffectMap.end())
    {
        Sound* sound = new Sound(path);
        soundEffectMap.insert(std::make_pair(name, sound));
        return sound;
    }
    else
    {
        return GetSound(name);
    }
}

Sound *ResourceManager::GetSound(std::string name)
{
    auto find = soundEffectMap.find(name);
    if(find != soundEffectMap.end())
    {
        return find->second;
    }

    return nullptr;
}

void ResourceManager::FreeSound(std::string name)
{
    auto find = soundEffectMap.find(name);
    if(find != soundEffectMap.end())
    {
        soundEffectMap.erase(find);
    }
}

Font *ResourceManager::LoadFont(std::string name, std::string path)
{
    if(fontMap.find(name) == fontMap.end())
    {
        Font* sound = new Font(path);
        fontMap.insert(std::make_pair(name, sound));
        return sound;
    }
    else
    {
        return GetFont(name);
    }
}

Font *ResourceManager::GetFont(std::string name)
{
    auto find = fontMap.find(name);
    if(find != fontMap.end())
    {
        return find->second;
    }

    return nullptr;
}

void ResourceManager::FreeFont(std::string name)
{
    auto find = fontMap.find(name);
    if(find != fontMap.end())
    {
        fontMap.erase(find);
    }
}
