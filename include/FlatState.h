//
// Created by Keith Webb on 11/28/15.
//

#pragma once

#include <Ava/Util/GameState.h>
#include <Ava/Util/EventReciever.h>
#include <Ava/Graphics/2D/Sprite.h>

class FlatState : public GameState, public EventReciever
{
public:

    FlatState();
    ~FlatState();

    virtual void create();
    virtual void update(float dt);
    virtual void render();
    virtual void event(std::string eventType, std::string eventValue);

private:
    Shader* shader2D;
    Camera2D camera;
    Sprite sprite;
};
