//
// Created by Keith Webb on 11/18/15.
//

#include <iostream>
#include <FlatState.h>
#include "MainState.h"

MainState::MainState() :
        model("models/nanosuit/nanosuit.obj"),
        directional(Light::DIRECTIONAL),
        spot(Light::SPOT),
        point(Light::POINT)
{
    testX = 0;
    m = false;
}

MainState::~MainState()
{
    InputManager::RemoveReciever(this);
    ResourceManager::FreeTexture("container");
    ResourceManager::FreeTexture("container_specular");
    ResourceManager::FreeTexture("matrix");
    ResourceManager::FreeTexture("face");
    ResourceManager::FreeMusic("music1");
    ResourceManager::FreeSound("sound1");
    ResourceManager::FreeShader("3D");
    ResourceManager::FreeShader("2D");
}

void MainState::create()
{
    InputManager::AddReciever(this);

    ResourceManager::LoadTexture("container_specular", "textures/container2_specular.png");
    ResourceManager::LoadTexture("container", "textures/container2.png");
    ResourceManager::LoadTexture("matrix", "textures/matrix.jpg");
    Texture* faceTex = ResourceManager::LoadTexture("face", "textures/face.png");

    renderTexture.create(faceTex->Width, faceTex->Height);

    sound = ResourceManager::LoadSound("sound1", "audio/se1.ogg");

    face.setTexture(faceTex);

    shader3D = ResourceManager::LoadShader("3D", "shaders/3d.vert", "shaders/3d.frag");
    shader2D = ResourceManager::LoadShader("2D", "shaders/2d.vert", "shaders/2d.frag");

    shader3D->Use();
    shader3D->SetMatrix4("projection", game->perspective_projection);

    for(int i = 0; i < 10; i++)
    {
        test[i].setDiffuseTexture(ResourceManager::GetTexture("container"));
        test[i].setSpecularTexture(ResourceManager::GetTexture("container_specular"));
    }
    test[3].setEmissionTexture(ResourceManager::GetTexture("matrix"));
    test[7].setEmissionTexture(ResourceManager::GetTexture("matrix"));

    for(int i = 0; i < 10; i++) {
        test[i].setPositionX(rand() % 6);
        test[i].setPositionY(rand() % 6);
        test[i].setPositionZ(rand() % 6);
        test[i].setRotationX(rand() % 360);
        test[i].setRotationY(rand() % 360);
        test[i].setRotationZ(rand() % 360);
    }

    test[0].setPosition(6, 6, 6);
    test[0].setRotation(0, 0, 0);

    spot.scalar(0.1f, 0.1f, 0.1f);

    //sprite.scalar(0.2f, 0.2f);
    //sprite.setPosition(400, 300);
    //sprite.setColor(255, 0, 0, 128);

    spot.setColorGL(0.8f, 0.8f, 0.8f, 1.0f);
    directional.setPosition(-0.2f, -1.0f, -0.3f);
    point.scalar(0.1f, 0.1f, 0.1f);

    model.scalar(0.2f, 0.2f, 0.2f);
    model.rotate(0, -90, 0);

    //font.loadFromFile("fonts/arial.ttf", 32);
    camera3D.translate(0.0f, 1.0f, 1.0f);

    sprite.setTexture(faceTex);
    sprite.setOriginCentered(faceTex->Width, faceTex->Height);
    sprite.scalar(0.2f, 0.2f);
    sprite.translate(64, 64);
    sprite.setColor(255, 255, 255, 128);
    //sprite.setOriginCentered(faceTex->Width, faceTex->Height);
}

void MainState::update(float dt)
{
    //sprite.rotate(50.0f * (dt / 1000));
    float camSpeed = (5 * (dt / 1000));

    if(InputManager::IsKeyDown("W"))
        camera3D.setPosition(camera3D.getPosition() + camSpeed * camera3D.cameraFront);
    if(InputManager::IsKeyDown("S"))
        camera3D.setPosition(camera3D.getPosition() - camSpeed * camera3D.cameraFront);
    if(InputManager::IsKeyDown("A"))
        camera3D.setPosition(camera3D.getPosition() - glm::normalize(glm::cross(camera3D.cameraFront, camera3D.cameraUp)) * camSpeed);
    if(InputManager::IsKeyDown("D"))
        camera3D.setPosition(camera3D.getPosition() + glm::normalize(glm::cross(camera3D.cameraFront, camera3D.cameraUp)) * camSpeed);
    if(InputManager::IsKeyDown("E"))
        camera3D.translate(0, camSpeed, 0);
    if(InputManager::IsKeyDown("Q"))
        camera3D.translate(0, -camSpeed, 0);

    if(InputManager::IsKeyDown("Up"))
        sprite.translate(0, -500.0f * (dt / 1000));
    if(InputManager::IsKeyDown("Down"))
        sprite.translate(0, 500.0f * (dt / 1000));
    if(InputManager::IsKeyDown("Left"))
        sprite.translate(-500.0f * (dt / 1000), 0);
    if(InputManager::IsKeyDown("Right"))
        sprite.translate(500.0f * (dt / 1000), 0);
    if(InputManager::IsKeyDown("M"))
        sprite.scalar(1.01f, 1.01f);
    if(InputManager::IsKeyDown("N"))
        sprite.scalar(0.99f, 0.99f);
    if(InputManager::IsKeyDown("B"))
        sprite.rotate(1);
    if(InputManager::IsKeyDown("V"))
        sprite.rotate(-1);

    if(InputManager::IsKeyDown("K"))
        rt += 1;
    if(InputManager::IsKeyDown("L"))
        rt -= 1;

    camera3D.update();
    camera2D.update();

    spot.setDirection(camera3D.cameraFront);
    spot.setPosition(camera3D.getPosition());

    if(testX < -4)
        m = false;
    if(testX > 4)
        m = true;

    if(m)
    {
        testX -= 0.1f;
    }
    else
    {
        testX += 0.1f;
    }

    point.setPositionX(testX);
}

void MainState::render()
{
    game->display.clear(0.2f, 0.2f, 0.2f);

    //Draw
    camera3D.apply(shader3D);

    for(int i = 0; i < 10; i++)
    {
        test[i].render(shader3D);
    }

    model.Draw(shader3D);

    spot.render(shader3D);
    point.render(shader3D);
    directional.render(shader3D);

    camera2D.apply(shader2D);
    //renderTexture.clear();
    //renderTexture.renderTo(face, shader2D);
    sprite.render(shader2D);
    //face.render(shader2D);

    //font.render(*shader2D, "Hello World!", glm::vec2(400.0f, 250.0f), rt, glm::vec2(1.0f, 1.0f), glm::vec2(0.0f, 0.0f), glm::vec4(1.0f, 1.0f, 1.0f, 1.0f));

    game->display.flip();
}

void MainState::event(std::string eventType, std::string eventValue)
{
    if(eventType == "MouseWheel")
    {
        if(fov >= 1.0f && fov <= 45.0f)
            fov -= std::stoi(eventValue);
        if(fov <= 1.0f)
            fov = 1.0f;
        if(fov >= 45.0f)
            fov = 45.0f;
        game->perspective_projection = glm::perspective(glm::radians(fov), ((float)Display::Width / (float)Display::Height), 0.1f, 100.0f);
        shader3D->SetMatrix4("projection", game->perspective_projection);
    }
    if(eventType == "KeyDown")
    {
        if(eventValue == "Escape")
        {
            game->stop();
        }

        if(eventValue == "Space")
        {
            sound->play();
        }
        if(eventValue == "1")
        {
            GameStateManager::ChangeState(new FlatState());
        }
        if(eventValue == "2")
        {
            InputManager::RemoveReciever(this);
        }
    }
}
