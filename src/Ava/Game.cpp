//
// Created by Keith Webb on 11/18/15.
//

#include <Ava/Game.h>
#include <Ava/Util/GameState.h>

glm::mat4 Game::perspective_projection;
glm::mat4 Game::orthographic_projection;

Game::Game(int width, int height, std::string title) : display(width, height, title)
{

}

Game::~Game()
{

}

void Game::start(GameState *state, bool ortho, float fov, float near, float far)
{
    orthographic_projection = glm::ortho(0.0f, (float)Display::Width, (float)Display::Height, 0.0f, -1.0f, 1.0f);
    //orthographic_projection = glm::ortho(0.0f, (float)Display::Width, 0.0f, (float)Display::Height, -1.0f, 1.0f);
    perspective_projection = glm::perspective(glm::radians(fov), ((float)Display::Width / (float)Display::Height), near, far);

    GameStateManager::game = this;

    GameStateManager::PushState(state);

    float last_time = SDL_GetTicks();
    while (running)
    {
        SDL_Event e;
        while(SDL_PollEvent(&e))
        {
            if (e.type == SDL_QUIT)
                running = false;
            InputManager::Event(e);
        }

        float this_time = SDL_GetTicks();
        float dt = this_time - last_time;
        last_time = this_time;

        Shader::Global_Perspective_Projection = perspective_projection;
        Shader::Global_Orthographic_Projection = orthographic_projection;

        GameStateManager::UpdateStates(dt);

        GameStateManager::RenderStates();
    }

    SDL_Quit();
}

void Game::stop()
{
    running = false;
}