//
// Created by Keith Webb on 11/18/15.
//

#include <Ava/Util/GameStateManager.h>
#include <Ava/Util/GameState.h>
#include <Ava/Game.h>
#include <Ava/Util/System.h>

std::stack<GameState*> GameStateManager::stack;
Game* GameStateManager::game = nullptr;

GameStateManager::GameStateManager()
{

}

GameStateManager::~GameStateManager()
{
    while(!stack.empty())
    {
        delete stack.top();
        stack.pop();
    }
}

void GameStateManager::PushState(GameState *state)
{
    state->game = game;
    stack.push(state);
    state->create();
}

void GameStateManager::PopState()
{
    delete stack.top();
    stack.pop();
}

void GameStateManager::ChangeState(GameState *state)
{
    PopState();
    PushState(state);
}

GameState *GameStateManager::PeekState()
{
    return stack.top();
}

void GameStateManager::UpdateStates(float dt)
{
    stack.top()->update(dt);
    System::FMOD_System->update();
}

void GameStateManager::RenderStates()
{
    stack.top()->render();
}
