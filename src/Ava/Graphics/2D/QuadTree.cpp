//
// Created by Keith Webb on 11/23/15.
//

#include <Ava/Graphics/2D/QuadTree.h>
#include <math.h>
#include <iostream>

int QuadTree::bucketSize = 25;

QuadTree::QuadTree(float x, float y, float width, float height, int bucketsize)
{
    bucketSize = bucketsize;
    root = new QNode(x, y, width, height);
    this->x = x;
    this->y = y;
    this->width = width;
    this->height = height;

    std::cerr << "The QuadTree Class is Broken!! Don't Use!!\n";
}

QuadTree::~QuadTree()
{
    delete root;
    root = nullptr;
}

void QuadTree::setBucketSize(int size)
{
    bucketSize = size;
}

int QuadTree::getBucketSzie()
{
    return bucketSize;
}

void QuadTree::update()
{
    root->update();
}

void QuadTree::add(IQTAble& toAdd)
{
    root->add(toAdd);
}

void QuadTree::remove(IQTAble& toRemove)
{
    root->remove(toRemove);
}

QuadTree::QNode::QNode(float x, float y, float width, float height) : area(x, y, width, height)
{
    child1 = child2 = child3 = child4 = nullptr;
    parent = nullptr;
}

QuadTree::QNode::~QNode()
{
    if(child1 != nullptr)
        delete child1;
    if(child2 != nullptr)
        delete child2;
    if(child3 != nullptr)
        delete child3;
    if(child4 != nullptr)
        delete child4;

    child1 = child2 = child3 = child4 = nullptr;
    parent = nullptr;
}

void QuadTree::QNode::update()
{
    if(haveChildren())
    {
        child1->update();
        child2->update();
        child3->update();
        child4->update();

        if(!child1->haveChildren() && !child2->haveChildren() && !child3->haveChildren() && !child4->haveChildren())
        {
            int sum = child1->getCount() + child2->getCount() + child3->getCount() + child4->getCount();
            if(sum < bucketSize)
            {
                superdivide();
            }
        }
    }
    else
    {
        for(int i = 0; i < bucket.size(); i++)
        {
            for(int j = i + 1; j < bucket.size(); j++)
            {
                IQTAble* obj1 = bucket[i];
                IQTAble* obj2 = bucket[j];

                float x1 = obj1->IgetPosX();
                float y1 = obj1->IgetPosY();
                float x2 = obj2->IgetPosX();
                float y2 = obj2->IgetPosY();

                float distance = sqrtf(powf(x2 - x1, 2) + powf(y2 - y1, 2));
                if(distance < obj1->IgetRadius() + obj2->IgetRadius())
                {
                    obj1->setCollided(true);
                    obj2->setCollided(true);
                }
            }
        }

        for (int k = 0; k < bucket.size(); k++)
        {
            auto rect = bucket[k]->IgetRect();
            if(!area.contains(&rect))
            {
                auto val = bucket[k];
                remove(*val);
                parent->add(*val);
            }
        }

    }
}

void QuadTree::QNode::add(IQTAble& toAdd)
{
    if(haveChildren())
    {
        passToChildren(toAdd);
    }
    else
    {
        if(bucket.size() > bucketSize)
        {
            subdivide();
            passToChildren(toAdd);
        }
        else
        {
            bucket.push_back(&toAdd);
        }
    }
}

void QuadTree::QNode::remove(IQTAble& toRemove)
{
    if(haveChildren())
    {
        child1->remove(toRemove);
        child2->remove(toRemove);
        child3->remove(toRemove);
        child4->remove(toRemove);
    }
    else
    {
        for(int i = 0; i < bucket.size(); i++)
        {
            if(bucket[i] == &toRemove)
            {
                bucket.erase(bucket.begin() + i);
            }
        }
    }
}

bool QuadTree::QNode::haveChildren()
{
    bool a, b, c, d;

    a = (child1 != nullptr);
    b = (child2 != nullptr);
    c = (child3 != nullptr);
    d = (child4 != nullptr);

    return (a && b && c && d);
}

void QuadTree::QNode::passToChildren(IQTAble& toPass)
{
    auto rect = toPass.IgetRect();
    if(child1->area.intersect(&rect))
    {
        child1->remove(toPass);
    }
    if(child2->area.intersect(&rect))
    {
        child2->remove(toPass);
    }
    if(child3->area.intersect(&rect))
    {
        child3->remove(toPass);
    }
    if(child4->area.intersect(&rect))
    {
        child4->remove(toPass);
    }
}

void QuadTree::QNode::subdivide()
{
    child1 = new QNode(area.getX(), area.getY(), area.getWidth() / 2, area.getHeight() / 2);
    child2 = new QNode(area.getX() + area.getWidth() / 2, area.getY(), area.getWidth() / 2, area.getHeight() / 2);
    child3 = new QNode(area.getX(), area.getY() + area.getHeight() / 2, area.getWidth() / 2, area.getHeight() / 2);
    child4 = new QNode(area.getX() + area.getWidth() / 2, area.getY() + area.getHeight() / 2, area.getWidth() / 2, area.getHeight() / 2);
    child1->parent = this;
    child2->parent = this;
    child3->parent = this;
    child4->parent = this;

    for(auto val : bucket)
    {
        passToChildren(*val);
    }

    bucket.clear();
}

void QuadTree::QNode::superdivide()
{
    for(auto val : child1->bucket)
    {
        bucket.push_back(val);
    }
    for(auto val : child2->bucket)
    {
        bucket.push_back(val);
    }
    for(auto val : child3->bucket)
    {
        bucket.push_back(val);
    }
    for(auto val : child4->bucket)
    {
        bucket.push_back(val);
    }

    child1->bucket.clear();
    child2->bucket.clear();
    child3->bucket.clear();
    child4->bucket.clear();

    delete child1;
    delete child2;
    delete child3;
    delete child4;

    child1 = child2 = child3 = child4 = nullptr;
}

int QuadTree::QNode::getCount()
{
    return (int) bucket.size();
}

void QuadTree::clear()
{
    delete root;
    root = new QNode(x, y, width, height);
}
