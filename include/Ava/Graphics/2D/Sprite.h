//
// Created by Keith Webb on 11/18/15.
//

#pragma once

#include <Ava/Graphics/Texture.h>
#include <Ava/Graphics/Shader.h>
#include <Ava/Graphics/Colorable.h>
#include <Ava/Graphics/2D/Transformable2D.h>
#include <Ava/Graphics/Drawable.h>

class Sprite : public Transformable2D, public Colorable, public Drawable
{
public:
    Sprite();
    Sprite(Texture *);
    ~Sprite();

    void setTexture(Texture *);
    Texture * getTexture();

    virtual void render(Shader* shader);

    int getWidth() { return mTexture->Width; }
    int getHeight() { return mTexture->Height; }

    GLuint getVBO() { return VBO; }
    GLuint getVAO() { return VAO; }

private:
    void setup();
    GLuint VBO, VAO;
    Texture * mTexture;
};
