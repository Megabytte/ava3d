//
// Created by Keith Webb on 11/24/15.
//

#pragma once

#include <Ava/Graphics/2D/QuadTree.h>
#include <random>

class Circle : public IQTAble, public Sprite
{
public:
    Circle()
    {
        setTexture(ResourceManager::GetTexture("circle"));
        float x = rand() % 800;
        float y = rand() % 600;
        positionV.x = x;
        positionV.y = y;
        float vx = -rand() % 80;
        float vy = rand() % 80;
        velocity.x = vx;
        velocity.y = vy;
    }

    void update(float dt)
    {
        positionV += velocity * (dt / 1000);

        if(positionV.x > 800)
        {
            positionV.x = 800;
            velocity.x = -velocity.x;
        }
        if(positionV.x < 0)
        {
            positionV.x = 0;
            velocity.x = -velocity.x;
        }
        if(positionV.y > 600)
        {
            positionV.y = 600;
            velocity.y = -velocity.y;
        }
        if(positionV.y < 0)
        {
            positionV.y = 0;
            velocity.y = -velocity.y;
        }

        if(ICollided)
        {
            setColor(128, 0, 255, 255);
        }
        else
        {
            setColor(255, 255, 255, 255);
        }
    }

private:
    glm::vec2 velocity;
};
