//
// Created by Keith Webb on 11/23/15.
//

#include <Ava/Audio/2D/Music.h>
#include <Ava/Util/System.h>

Music::Music()
{
    fmod_channel = nullptr;
    fmod_handle = nullptr;
    ready = false;
}

Music::Music(std::string path)
{
    loadMusic(path);
}

Music::~Music()
{
    fmod_channel->stop();
    fmod_handle->release();

    fmod_channel = nullptr;
    fmod_handle = nullptr;
    ready = false;
}

void Music::loadMusic(std::string path)
{
    System::FMOD_System->createStream(path.c_str(), FMOD_DEFAULT, 0, &fmod_handle);
    ready = true;
}

void Music::play(int repeats)
{
    if(ready) {
        fmod_handle->setLoopCount(repeats);
        System::FMOD_System->playSound(fmod_handle, 0, false, &fmod_channel);
    }
}

void Music::pause()
{
    if(ready) {
        fmod_channel->setPaused(true);
    }
}

void Music::resume()
{
    if(ready) {
        fmod_channel->setPaused(false);
    }
}

void Music::stop()
{
    if(ready) {
        fmod_channel->stop();
    }
}

bool Music::IsMusicPlaying()
{
    if(ready) {
        bool r = false;
        fmod_channel->isPlaying(&r);
        return r;
    } else {
        return false;
    }
}

bool Music::IsMusicPaused() {
    if(ready) {
        bool r = false;
        fmod_channel->getPaused(&r);
        return r;
    } else {
        return false;
    }
}
