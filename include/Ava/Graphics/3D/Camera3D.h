//
// Created by Keith Webb on 11/24/15.
//

#pragma once

#include <Ava/Graphics/3D/Transformable3D.h>
#include <Ava/Graphics/Shader.h>

class Camera3D : public Transformable3D
{
public:
    Camera3D();
    Camera3D(float x, float y, float z, float width, float height, float near, float far);
    ~Camera3D();

    void set(float x, float y, float z, float width, float height, float near, float far);

    void update();

    void apply(Shader* shader);

    glm::vec3 cameraFront;
    glm::vec3 cameraUp;
    glm::mat4 view;

private:
    float lmx, lmy;
    float width, height;
    float near, far;
    bool firstMouse = true;

    float pitch = 0, yaw = 0;
};
