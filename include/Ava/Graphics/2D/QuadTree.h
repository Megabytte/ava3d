//
// Created by Keith Webb on 11/23/15.
//

#pragma once

#include <vector>
#include <Ava/Graphics/2D/Rectangle.h>

class IQTAble
{
public:
    Rectangle<float> IgetRect() { return Irect; }
    float IgetRadius() { return Iradius; }
    float IgetPosX() { return Irect.getX() + Iradius; }
    float IgetPosY() { return Irect.getY() + Iradius; }
    void setCollided(bool c) { ICollided = c; }
    bool getCollided() { return ICollided; }

protected:
    Rectangle<float> Irect;
    float Iradius;
    bool ICollided;
};

class QuadTree
{
public:
    QuadTree(float x, float y, float width, float height, int bucketSize = 25);
    ~QuadTree();

    void update();

    void add(IQTAble& toAdd);
    void remove(IQTAble& toRemove);

    void setBucketSize(int size);
    int getBucketSzie();

    void clear();

private:
    class QNode
    {
    public:
        QNode(float x, float y, float width, float height);
        ~QNode();

        void update();
        void add(IQTAble& toAdd);
        void remove(IQTAble& toRemove);

    private:

        Rectangle<float> area;

        void subdivide();
        void superdivide();

        bool haveChildren();
        void passToChildren(IQTAble& toPass);

        int getCount();

        std::vector<IQTAble*> bucket;

        QNode* parent;
        QNode* child1;
        QNode* child2;
        QNode* child3;
        QNode* child4;
    };

    QNode* root;
    static int bucketSize;

    float x, y, width, height;
};
