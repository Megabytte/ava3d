//
// Created by Keith Webb on 11/29/15.
//

#pragma once

#include <Ava/Graphics/Shader.h>

class Drawable
{
public:
    virtual void render(Shader* shader) = 0;
};
