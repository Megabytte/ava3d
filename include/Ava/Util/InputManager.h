//
// Created by Keith Webb on 11/18/15.
//

#pragma once

#include <string>
#include <vector>
#include <unordered_map>
#include <SDL2/SDL.h>
#include <glm/detail/type_vec.hpp>
#include "EventReciever.h"

class EventReciever;

class InputManager
{
public:
    static void AddReciever(EventReciever*);
    static void RemoveReciever(EventReciever*);

    static bool IsKeyDown(std::string);
    static void Event(SDL_Event);

    static bool IsMBDown(std::string);

    static glm::vec2 GetMousePosition();

private:
    InputManager();
    ~InputManager();
    static bool remove(EventReciever* one, EventReciever* two);
    static std::vector<EventReciever*> recievers;
    static std::unordered_map<std::string, bool> keys;
    static bool leftMB;
    static bool middleMB;
    static bool rightMB;
};
