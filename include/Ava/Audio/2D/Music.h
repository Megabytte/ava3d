//
// Created by Keith Webb on 11/23/15.
//

#pragma once

#include <string>
#include <fmodex/fmod.hpp>

class Music
{
public:
    Music();
    Music(std::string path);
    ~Music();

    void loadMusic(std::string path);

    void play(int repeats = 0);
    void pause();
    void resume();
    void stop();

    bool IsMusicPlaying();
    bool IsMusicPaused();

private:
    bool ready;
    FMOD::Sound *fmod_handle;
    FMOD::Channel* fmod_channel;
};
