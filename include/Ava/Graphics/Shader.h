#pragma once

#include <string>

#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>

class Shader
{
public:
    static glm::mat4 Global_Perspective_Projection;
    static glm::mat4 Global_Orthographic_Projection;

    GLuint ID;
    Shader() { }
    ~Shader() { glDeleteShader(ID); }
    Shader &Use();
    void CompileFromFile(const GLchar *vertexPath, const GLchar *fragmentPath, const GLchar *geometryPath = nullptr);
    void CompileFromSource(const GLchar *vertexSource, const GLchar *fragmentSource, const GLchar *geometrySource = nullptr);

    void SetBool (const GLchar *name, GLboolean value, GLboolean useShader = (GLboolean) false);
    void SetFloat (const GLchar *name, GLfloat value, GLboolean useShader = (GLboolean) false);
    void SetInteger (const GLchar *name, GLint value, GLboolean useShader = (GLboolean) false);
    void SetVector2f (const GLchar *name, GLfloat x, GLfloat y, GLboolean useShader = (GLboolean) false);
    void SetVector2f (const GLchar *name, const glm::vec2 &value, GLboolean useShader = (GLboolean) false);
    void SetVector3f (const GLchar *name, GLfloat x, GLfloat y, GLfloat z, GLboolean useShader = (GLboolean) false);
    void SetVector3f (const GLchar *name, const glm::vec3 &value, GLboolean useShader = (GLboolean) false);
    void SetVector4f (const GLchar *name, GLfloat x, GLfloat y, GLfloat z, GLfloat w, GLboolean useShader = (GLboolean) false);
    void SetVector4f (const GLchar *name, const glm::vec4 &value, GLboolean useShader = (GLboolean) false);
    void SetMatrix4 (const GLchar *name, const glm::mat4 &matrix, GLboolean useShader = (GLboolean) false);
private:
    void checkCompileErrors(GLuint object, std::string type);
};

