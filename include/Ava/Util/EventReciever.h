//
// Created by Keith Webb on 11/18/15.
//

#pragma once

#include <string>

class EventReciever
{
public:

    virtual void event(std::string eventType, std::string eventValue) = 0;

};