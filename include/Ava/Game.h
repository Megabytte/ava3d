//
// Created by Keith Webb on 11/18/15.
//

#pragma once

#include <string>
#include <glm/gtc/matrix_transform.hpp>
#include <Ava/Util/ResourceManager.h>
#include <Ava/Util/InputManager.h>
#include <Ava/Util/GameStateManager.h>
#include <Ava/Graphics/Display.h>
#include <Ava/Graphics/2D/Camera2D.h>

class GameState;

class Game
{
public:
    Game(int width, int height, std::string title);
    ~Game();

    void start(GameState*, bool ortho, float fov, float near, float far);
    void stop();

    Display display;

    static glm::mat4 perspective_projection;
    static glm::mat4 orthographic_projection;

private:
    bool running = true;
};
