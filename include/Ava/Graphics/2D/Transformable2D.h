//
// Created by Keith Webb on 11/18/15.
//

#pragma once

#include <glm/detail/type_vec.hpp>
#include <glm/detail/type_vec2.hpp>
#include <glm/gtc/matrix_transform.hpp>

class Transformable2D
{
public:
    Transformable2D()
    {
        position.x = position.y = 0.0f;
        center.x = center.y = 0.0f;
        scale.x = scale.y = 1.0f;
        rotation = 0;
    }

    void setPosition(float lx, float ly)
    {
        this->position.x = lx;
        this->position.y = ly;
    }
    void setPositionX(float lx)
    {
        this->position.x = lx;
    }
    void setPositionY(float ly)
    {
        this->position.y = ly;
    }
    void setScale(float lsx, float lsy)
    {
        this->scale.x = lsx;
        this->scale.y = lsy;
    }
    void setScaleX(float lsx)
    {
        this->scale.x = lsx;
    }
    void setScaleY(float lsy)
    {
        this->scale.y = lsy;
    }
    void setRotation(float rot)
    {
        this->rotation = rot;
    }

    void setCenter(float lcx, float lcy)
    {
        center.x = lcx;
        center.y = lcy;
    }
    void setOriginCentered(int width, int height)
    {
        center.x = (width / 2.0f);
        center.y = (height / 2.0f);
    }
    void setCenterX(float lcx)
    {
        this->center.x = lcx;
    }
    void setCenterY(float lcy)
    {
        this->center.y = lcy;
    }

    float getPositionX()
    {
        return position.x;
    }
    float getPositionY()
    {
        return position.y;
    }
    float getScaleX()
    {
        return scale.x;
    }
    float getScaleY()
    {
        return scale.y;
    }
    float getCenterX()
    {
        return center.x;
    }
    float getCenterY()
    {
        return center.y;
    }

    void translate(float lx, float ly)
    {
        this->position.x += lx;
        this->position.y += ly;
    }
    void rotate(float rot)
    {
        this->rotation += rot;
    }
    void scalar(float lsx, float lsy)
    {
        this->scale.x *= lsx;
        this->scale.y *= lsy;
    }

    glm::mat4 getTransform()
    {
        glm::mat4 model;

        model = glm::translate(model, glm::vec3(position.x - center.x, position.y - center.y, 0.0f));

        model = glm::translate(model, glm::vec3(center.x, center.y, 0.0f));
        model = glm::scale(model, glm::vec3(scale, 1.0f));
        model = glm::rotate(model, glm::radians(rotation), glm::vec3(0.0f, 0.0f, 1.0f));
        model = glm::translate(model, glm::vec3(-center.x, -center.y, 0.0f));

        return model;
    }

    glm::vec2 getPosition() { return position; }
    glm::vec2 getCenter() { return center; }
    glm::vec2 getScale() { return scale; }
    float getRotation() { return rotation; }

protected:
    glm::vec2 position, scale, center;
    float rotation;
};
