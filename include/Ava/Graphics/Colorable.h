//
// Created by Keith Webb on 11/20/15.
//

#pragma once

#include <glm/detail/type_vec.hpp>
#include <glm/detail/type_vec4.hpp>
#include <GL/glew.h>

class Colorable
{
public:
    Colorable()
    {
        color.r = color.g = color.b = 0;
        color.a = 1;
    }

    void setColor(unsigned char red, unsigned char green, unsigned char blue, unsigned char alpha)
    {
        color.r = (float)red / (float)255;
        color.g = (float)green / (float)255;
        color.b = (float)blue / (float)255;
        color.a = (float)alpha / (float)255;
    }
    void setColorGL(float red, float green, float blue, float alpha)
    {
        color.r = red;
        color.g = green;
        color.b = blue;
        color.a = alpha;
    }
    void setRed(unsigned char red)
    {
        color.r = (float)red / (float)255;
    }
    void setGreen(unsigned char green)
    {
        color.g = (float)green / (float)255;
    }
    void setBlue(unsigned char blue)
    {
        color.b = (float)blue / (float)255;
    }
    void setAlpha(unsigned char alpha)
    {
        color.a = (float)alpha / (float)255;
    }

    unsigned char getCharRed()
    {
        return (unsigned char) (color.r * (float)255);
    }
    unsigned char getCharGreen()
    {
        return (unsigned char) (color.g * (float)255);
    }
    unsigned char getCharBlue()
    {
        return (unsigned char) (color.b * (float)255);
    }
    unsigned char getCharAlpha()
    {
        return (unsigned char) (color.a * (float)255);
    }

    GLfloat getGLRed()
    {
        return color.r;
    }
    GLfloat getGLBlue()
    {
        return color.g;
    }
    GLfloat getGLGreen()
    {
        return color.b;
    }
    GLfloat getGLAlpha()
    {
        return color.a;
    }

    glm::vec4 getColor() { return color; }


protected:
    glm::vec4 color;
};