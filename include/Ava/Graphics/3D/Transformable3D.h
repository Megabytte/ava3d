//
// Created by Keith Webb on 11/24/15.
//

#pragma once

#include <glm/detail/type_vec.hpp>
#include <glm/fwd.hpp>
#include <glm/detail/type_vec3.hpp>
#include <glm/gtc/quaternion.hpp>
#include <iostream>
#include <glm/gtc/matrix_transform.hpp>

class Transformable3D
{
public:
    Transformable3D()
    {
        position.x = position.y = position.z = 0.0f;
        center.x = center.y = center.z = 0.0f;
        scale.x = scale.y = scale.z = 1.0f;
        rotationVector.x = rotationVector.y = rotationVector.z = 0;
    }

    void setPosition(float lx, float ly, float lz)
    {
        this->position.x = lx;
        this->position.y = ly;
        this->position.z = lz;
    }
    void setPositionX(float lx)
    {
        this->position.x = lx;
    }
    void setPositionY(float ly)
    {
        this->position.y = ly;
    }
    void setPositionZ(float lz)
    {
        this->position.z = lz;
    }
    void setScale(float lsx, float lsy, float lsz)
    {
        this->scale.x = lsx;
        this->scale.y = lsy;
        this->scale.z = lsz;
    }
    void setScaleX(float lsx)
    {
        this->scale.x = lsx;
    }
    void setScaleY(float lsy)
    {
        this->scale.y = lsy;
    }
    void setScaleZ(float lsz)
    {
        this->scale.z = lsz;
    }

    void setCenter(float lcx, float lcy, float lcz)
    {
        center.x = lcx;
        center.y = lcy;
        center.z = lcz;
    }
    void setCenterX(float lcx)
    {
        this->center.x = lcx;
    }
    void setCenterY(float lcy)
    {
        this->center.y = lcy;
    }
    void setCenterZ(float lcz)
    {
        this->center.z = lcz;
    }
    void setRotation(float lrx, float lry, float lrz)
    {
        this->rotationVector.x = lrx;
        this->rotationVector.y = lry;
        this->rotationVector.z = lrz;
        updateQuaternion();
    }
    void setRotationX(float lrx)
    {
        this->rotationVector.x = lrx;
        updateQuaternion();
    }
    void setRotationY(float lry)
    {
        this->rotationVector.y = lry;
        updateQuaternion();
    }
    void setRotationZ(float lrz)
    {
        this->rotationVector.z = lrz;
        updateQuaternion();
    }

    float getPositionX()
    {
        return position.x;
    }
    float getPositionY()
    {
        return position.y;
    }
    float getPositionZ()
    {
        return position.z;
    }
    float getScaleX()
    {
        return scale.x;
    }
    float getScaleY()
    {
        return scale.y;
    }
    float getScaleZ()
    {
        return scale.z;
    }
    float getCenterX()
    {
        return center.x;
    }
    float getCenterY()
    {
        return center.y;
    }
    float getCenterZ()
    {
        return center.z;
    }
    float getRotationX()
    {
        return rotationVector.x;
    }
    float getRotationY()
    {
        return rotationVector.y;
    }
    float getRotationZ()
    {
        return rotationVector.z;
    }

    void translate(float lx, float ly, float lz)
    {
        this->position.x += lx;
        this->position.y += ly;
        this->position.z += lz;
    }
    void scalar(float lsx, float lsy, float lsz)
    {
        this->scale.x *= lsx;
        this->scale.y *= lsy;
        this->scale.z *= lsz;
    }
    void rotate(float lrx, float lry, float lrz)
    {
        rotationVector.x += lrx;
        rotationVector.y += lry;
        rotationVector.z += lrz;

        updateQuaternion();
    }

    void updateQuaternion()
    {
        glm::quat qx = glm::angleAxis(glm::radians(rotationVector.x), glm::vec3(1.0f, 0.0f, 0.0f));
        glm::quat qy = glm::angleAxis(glm::radians(rotationVector.y), glm::vec3(0.0f, 1.0f, 0.0f));
        glm::quat qz = glm::angleAxis(glm::radians(rotationVector.z), glm::vec3(0.0f, 0.0f, 1.0f));

        rotation = qx * qy * qz;
    }
    void updateRotationVector()
    {
        rotationVector = glm::eulerAngles(rotation);
    }

    glm::mat4 getTransform()
    {
        glm::mat4 model;

        model = glm::translate(model, position - center);

        model = glm::translate(model, -center);
        model = glm::scale(model, scale);
        model *= glm::mat4_cast(rotation);
        model = glm::translate(model, center);

        return model;
    }

    glm::vec3 getPosition() { return position; }
    glm::vec3 getCenter() { return center; }
    glm::vec3 getScale() { return scale; }
    glm::quat getRotation() { return rotation; }
    glm::vec3 getRotationVector() { return rotationVector; }

    void setPosition(glm::vec3 v) { position = v; }
    void setCenter(glm::vec3 v) { center = v; }
    void setScale(glm::vec3 v) { scale = v; }
    void setRotation(glm::quat q) { rotation = q; updateRotationVector(); }
    void setRotationVector(glm::vec3 v) { rotationVector = v; updateQuaternion(); }

protected:
    glm::vec3 position;
    glm::vec3 scale;
    glm::vec3 center;
    glm::quat rotation;
    glm::vec3 rotationVector;
};
