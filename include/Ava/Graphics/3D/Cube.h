//
// Created by Keith Webb on 11/25/15.
//

#pragma once

#include <Ava/Graphics/3D/Transformable3D.h>
#include <Ava/Graphics/Colorable.h>
#include <GL/glew.h>
#include <Ava/Graphics/Texture.h>
#include <Ava/Graphics/Shader.h>

class Cube : public Transformable3D, public Colorable
{
public:
    Cube();
    ~Cube();

    void render(Shader* shader);

    void setDiffuseTexture(Texture *texture);
    void setSpecularTexture(Texture *texture);
    void setEmissionTexture(Texture *texture);

private:
    bool canRender = false;
    GLuint VAO, VBO;
    Texture* texture, *specular = nullptr, *emission = nullptr;
};
