//
// Created by Keith Webb on 11/18/15.
//

#pragma once

#include <Ava/Game.h>

class Game;

class GameState
{
public:

    virtual ~GameState() {}

    Game* game;

    virtual void create() = 0;
    virtual void update(float dt) = 0;
    virtual void render() = 0;
};
